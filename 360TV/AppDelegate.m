//
//  AppDelegate.m
//  360TV
//
//  Created by Kirill Gorbushko on 16.03.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "AppDelegate.h"
#import "GiropticHomeVC.h"
#import "JDSideMenu.h"
#import "GiropticLeftMenuVC.h"
#import "GiropticSplitVC_iPad.h"
#import "CameraNetworkManager.h"
#import "UIWindow+UIActivityIndicator.h"
#import "ImageCacheManager.h"
#import "MainPreviewViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "WiFiFetchService.h"
#import "SVProgressHUD.h"
#import "UIAlertView+Hide.h"

@implementation AppDelegate

#pragma mark - LifeCycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self shouldDisableIddleTimer:YES];
    [self prepareInterface];
    [self registerObservers];
    [[WiFiFetchService sharedService] startNotifications];

    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [self closeSession];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self closeSession];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [UIAlertView hideAllAlerts];
    
    [self tryToRestoreSession];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    [[ImageCacheManager sharedManager] clearAllCache];
}

#pragma mark - Public

+ (NSString *)appName
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleNameKey];
}

#pragma mark - Observers

- (void)registerObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceDidConnectedToCamera) name:DeviceDidConnectedToCameraNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceDidDisconnectedFromCamera) name:DeviceDidDisconnectedToCameraNotification object:nil];
}

- (void)deviceDidConnectedToCamera
{
    NSLog(@"Device connected");
    [self prepareInterface];
}

- (void)deviceDidDisconnectedFromCamera
{
    NSLog(@"device Disconnected");
    [self prepareInterface];
}

#pragma mark - Private

- (void)prepareInterface
{
    [SVProgressHUD dismiss];
//    [CameraNetworkManager sharedManager].sessionID = @"ASDF";

#warning - TO Uncomment when BUILD for iphones
    if ([[CameraNetworkManager sharedManager] isCameraConnected]) {
        [self selectUIForConnectedMode];
    } else {
        [self selectUIForPreviewMode];
    }
}

- (void)shouldDisableIddleTimer:(BOOL)shouldDisable
{
    [UIApplication sharedApplication].idleTimerDisabled = shouldDisable;
}

- (void)selectUIForConnectedMode
{
    (isIPad()) ? [self showiPadStartScreenForConnectionMode] : [self showiPhoneStartScreenForConnectionMode];
}

- (void)selectUIForPreviewMode
{
    (isIPad()) ? [self showiPadStartScreenForPreviewMode] : [self showiPhoneStartScreenForPreviewMode];
}

- (void)showiPadStartScreenForConnectionMode
{
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPad" bundle:nil];
    GiropticSplitVC_iPad *splitVC = [storyboard instantiateViewControllerWithIdentifier:@"GiropticSplitVC_iPad"];
    self.detailViewManager = [[DetailViewManager alloc] init];
    self.detailViewManager.splitViewController = splitVC;
    GiropticHomeVC *homeVC = [storyboard instantiateViewControllerWithIdentifier:@"GiropticHomeVC"];
    UINavigationController *navc = [[UINavigationController alloc] initWithRootViewController:homeVC];
    navc.navigationBarHidden = YES;
    self.detailViewManager.detailViewController = (id) navc;
    splitVC.delegate = self.detailViewManager;
    
    if ([splitVC respondsToSelector:@selector(setPresentsWithGesture:)]) {
        [splitVC setPresentsWithGesture:YES];
    }
    [self setRootViewController:splitVC];
}

- (void)showiPhoneStartScreenForConnectionMode
{
    [self showPreviewStartScreenWithLeftStoryboardNamed:@"Storyboard_iPhone" mainStoryboardName:@"Storyboard_iPhone" mainVCIdentifier:@"HomeVC"];
}

- (void)showiPhoneStartScreenForPreviewMode
{
    [self showPreviewStartScreenWithLeftStoryboardNamed:@"Storyboard_iPhone" mainStoryboardName:@"Storyboard_preview_iPhone" mainVCIdentifier:@"mainPreview"];
}

- (void)showiPadStartScreenForPreviewMode
{
    [self showPreviewStartScreenWithLeftStoryboardNamed:@"Storyboard_iPad" mainStoryboardName:@"Storyboard_preview_iPad" mainVCIdentifier:@"MainPreviewViewController"];
}

- (void)showPreviewStartScreenWithLeftStoryboardNamed:(NSString *)leftSourceStoryboardName mainStoryboardName:(NSString *)mainStoryboard mainVCIdentifier:(NSString *)mainVCIdentifier
{
    UIStoryboard *storyboardLeft = [UIStoryboard storyboardWithName:leftSourceStoryboardName bundle:nil];
    GiropticLeftMenuVC *leftMenuVC = [storyboardLeft instantiateViewControllerWithIdentifier:@"LeftMenuVC"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:mainStoryboard bundle:nil];
    MainPreviewViewController *mainPreview = [storyboard instantiateViewControllerWithIdentifier:mainVCIdentifier];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mainPreview];
    navigationController.navigationBarHidden = YES;
    
    JDSideMenu *sideMenu = [[JDSideMenu alloc] initWithContentController:navigationController menuController:leftMenuVC];
    [self setRootViewController:sideMenu];
}

- (void)setRootViewController:(UIViewController *)viewController
{
    self.window.rootViewController = viewController;
    [self.window makeKeyAndVisible];
}

#pragma mark - APIUsage

- (void)tryToRestoreSession
{
    if ([[CameraNetworkManager sharedManager] isCameraConnected]) {
        if ([CameraNetworkManager sharedManager].sessionID.length) {
            [self.window showIndicatorWithText:@"Connecting to Camera"];
            __weak AppDelegate *weakSelf = self;
            [[CameraNetworkManager sharedManager] giropticUpdateSessionWithTimeOut:DefaultSessionDuration sessionId:[CameraNetworkManager sharedManager].sessionID operationResult:^(BOOL success, id response, NSError *error) {
                if (success) {
                    NSLog(@"Session successfuly updated");
                } else {
                    [CameraNetworkManager sharedManager].sessionID = @"";
                }
                [self selectUIForConnectedMode];
                [weakSelf.window hideIndicator];
            }];
        } else {
            [self selectUIForConnectedMode];
        }
    } else {
        [self selectUIForPreviewMode];
    }
}

- (void)closeSession
{
    [self.window hideIndicator];
    [[CameraNetworkManager sharedManager] giropticCloseSessionWithSessionId:nil operationResult:^(BOOL success, id response, NSError *error) {
        if (success) {
            NSLog(@"Successfuly closed session");
        }
    }];
}

#pragma mark - Rotation

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if (isIPad() && [[CameraNetworkManager sharedManager] isCameraConnected]) {
        return UIInterfaceOrientationMaskLandscapeLeft;
    } else if (isIPad() || [[self topViewController] isKindOfClass:[MPMoviePlayerViewController class]]) {
        return UIInterfaceOrientationMaskAll;
    } else {
        UIViewController *currentViewController = [self topViewController];
        if ([currentViewController respondsToSelector:NSSelectorFromString(@"canRotate")]) {
            return UIInterfaceOrientationMaskAll;
        }
        return UIInterfaceOrientationMaskPortrait;
    }
}

- (UIViewController*)topViewController
{
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController
{
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController *presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

@end