//
//  AppDelegate.h
//  360TV
//
//  Created by Kirill Gorbushko on 16.03.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "DetailViewManager.h"

static NSInteger const DefaultSessionDuration = 3600;

@class DetailViewManager;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) DetailViewManager *detailViewManager;

+ (NSString *)appName;

@end
