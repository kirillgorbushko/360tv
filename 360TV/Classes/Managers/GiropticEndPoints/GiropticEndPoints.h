//
//  GiropticEndPoints.h
//  testCamera360Api
//
//  Created by Kirill Gorbushko on 19.03.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#pragma mark - Giroptic API

static NSString *const GiroBaseURL = @"http://192.168.1.168/osc/commands/execute";
static NSString *const GiroBSSIDMACPart = @"ec:ba:fe";
static NSString *const GiroImageFilePath = @"/mnt/mmc/DCIM/360CAM";

static NSString *const GiroCaptureModeVideo = @"_video";
static NSString *const GiroCaptureModeImage = @"image";
static NSString *const GiroCaptureModeBurst = @"_burst";
static NSString *const GiroCaptureModeTimeLapse = @"_timelapse";
static NSString *const GiroCaptureModeLive = @"_live";

static NSString *const GiroStreamTypeURLMain = @"main";
static NSString *const GiroStreamTypeURLPreview = @"preview";

static NSString *const GiroWriteModeAppend = @"append";
static NSString *const GiroWriteModeOverwrite = @"overwrite";

#pragma mark - PreviewGiroAPI

static NSString *const PreviewGiroAPI = @"http://api.360.tv/app.json";
static NSString *const PreviewGiroAPIBase = @"http://api.360.tv/";
static NSString *const PreviewPresentationURL = @"http://player.vimeo.com/external/95807968.hd.mp4?s=affdeb764f5911e00c68c021bd5c11f4";

#pragma mark - AlbumsNames

static NSString *const AlbumNameGiropticLocalFies = @"360cam Photos";
static NSString *const AlbumNameGiropticLittlePlanetLocalFies = @"360cam Little Planet Photos";
static NSString *const PhotoFileDefaultName = @"GIR000001.JPG";

#pragma mark - InternalErrors

static NSInteger const GiroErrorIncorrectOperationRequest = 999001;
