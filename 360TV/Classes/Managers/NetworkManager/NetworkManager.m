//
//  NetworkManager.m
//  360cam
//
//  Created by Kirill Gorbushko on 03.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "NetworkManager.h"
#import "AFNetworking.h"
#import "GiropticEndPoints.h"

@interface NetworkManager()

@property (strong, nonatomic) AFHTTPRequestOperationManager *manager;

@end

@implementation NetworkManager

#pragma mark - Public

#pragma mark - Singleton

+ (instancetype)sharedManager
{
    static NetworkManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[NetworkManager alloc] init];
    });
    return sharedManager;
}

- (void)giropticGetImageWithPath:(NSString *)imagePath withCompletition:(DownloadingComplete)completitionHandler;
{
    [self.manager GET:imagePath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        UIImage *image = [UIImage imageWithData:responseObject];
        if (image) {
            completitionHandler(YES, image);
        } else {
            completitionHandler(NO, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        completitionHandler(NO, nil);
    }];
}

#pragma mark - LifeCycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self managerSetup];
    }
    return self;
}

#pragma mark - Private

#pragma mark - Setups

- (void)managerSetup
{
    self.manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:PreviewGiroAPIBase]];
    self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.allowInvalidCertificates = YES;
    self.manager.securityPolicy = securityPolicy;
}


@end
