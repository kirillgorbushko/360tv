//
//  NetworkManager.h
//  360cam
//
//  Created by Kirill Gorbushko on 03.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^DownloadingComplete)(BOOL success, UIImage *response);

@interface NetworkManager : NSObject

+ (instancetype)sharedManager;

- (void)giropticGetImageWithPath:(NSString *)imagePath withCompletition:(DownloadingComplete)completitionHandler;

@end
