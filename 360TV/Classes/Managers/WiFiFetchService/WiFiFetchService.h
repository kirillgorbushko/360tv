//
//  WiFiFetchService
//  360cam
//
//  Created by Kirill Gorbushko on 10.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *const DeviceDidConnectedToCameraNotification = @"DeviceDidConnectedToCameraNotification";
static NSString *const DeviceDidDisconnectedToCameraNotification = @"DeviceDidDisconnectedToCameraNotification";

typedef NS_ENUM (NSUInteger, FetchServiceStatus) {
    FetchServiceStatusDisconnected,
    FetchServiceStatusConnected
};

@interface WiFiFetchService : NSObject

+ (instancetype)sharedService;

- (void)startNotifications;
- (void)stopNotifications;

@property (assign, nonatomic, readonly) FetchServiceStatus currentStatus;
@property (strong, nonatomic, readonly) NSString *cameraName;

@end
