//
//  CoreLocationManager.h
//  testCoreLocation
//
//  Created by Kirill Gorbushko on 06.02.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

typedef void(^AccessResult)(BOOL status, NSString *message);

@class CLLocation;

@protocol LocationManagerDelegate <NSObject>

@optional
- (void)locationDidChangedTo:(CGFloat)longtitude lat:(CGFloat)latitude;
- (void)locationDidFailWithError:(NSError *)failError;

@end

@interface LocationManager : NSObject

@property (weak, nonatomic) id <LocationManagerDelegate> delegate;

@property (assign, nonatomic, readonly) CGFloat currentLongtitude;
@property (assign, nonatomic, readonly) CGFloat currentLattitude;
@property (assign, nonatomic, readonly) BOOL isLocationCaptured;

+ (instancetype)sharedManager;

- (BOOL)isLocationServiceEnabled;
- (void)permissionRequest;
- (void)checkLocationPermissions:(AccessResult)result;

- (void)stopUpdatingLocation;
- (void)startUpdatingLocation;

@end
