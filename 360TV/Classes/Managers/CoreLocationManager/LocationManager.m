//
//  CoreLocationManager.m
//  testCoreLocation
//
//  Created by Kirill Gorbushko on 06.02.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "LocationManager.h"
#import <CoreLocation/CoreLocation.h>

@interface LocationManager() <CLLocationManagerDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;

@end

@implementation LocationManager

#pragma mark - Public

+ (instancetype)sharedManager
{
    static LocationManager *sharedmanager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedmanager = [[LocationManager alloc] init];
    });
    return sharedmanager;
}

- (void)permissionRequest
{
    if ([[LocationManager sharedManager] isLocationServiceEnabled]) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
            [self.locationManager requestWhenInUseAuthorization];
        }
    } else {
      //  [UIAlertView showNotificationAlertWithTag:100 message:NSLocalizedString(@"kAlertNoPermissionLocationService", @"") buttonTitle:@"Setting" otherButton:nil delegate:self];
    }
}

- (BOOL)isLocationServiceEnabled
{
    return [CLLocationManager locationServicesEnabled];
}

- (void)stopUpdatingLocation
{
    if (self.locationManager) {
        [self.locationManager stopUpdatingLocation];
    }
}

- (void)startUpdatingLocation
{
    if (self.locationManager) {
        _isLocationCaptured = NO;
        [self.locationManager startUpdatingLocation];
    }
}

- (void)checkLocationPermissions:(AccessResult)result
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if (status == kCLAuthorizationStatusDenied) {
        result(NO, NSLocalizedString(@"kAlertNoPermissionLocation", @""));
    }
    else if (status == kCLAuthorizationStatusNotDetermined) {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
            [self.locationManager requestWhenInUseAuthorization];
        } else {
            [self.locationManager startUpdatingLocation];
        }
    } else if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusAuthorizedAlways) {
        [self.locationManager startUpdatingLocation];
        result(YES, nil);
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:settingsURL];
}

#pragma mark - Configuration

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self prepareLocationMamager];
    }
    return self;
}

- (void)prepareLocationMamager
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.distanceFilter = kCLHeadingFilterNone;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [self.locationManager requestWhenInUseAuthorization];
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation* location = [locations lastObject];
    
    _currentLattitude = location.coordinate.latitude;
    _currentLongtitude = location.coordinate.longitude;
    _isLocationCaptured = YES;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(locationDidChangedTo:lat:)]) {
        [self.delegate locationDidChangedTo:location.coordinate.longitude lat:location.coordinate.latitude];
    }
    [[LocationManager sharedManager] stopUpdatingLocation];
}

 -(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(locationDidFailWithError:)]) {
        [self.delegate locationDidFailWithError:error];
    }
}

@end
