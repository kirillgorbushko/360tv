//
//  ImageCacheManager.h
//  360cam
//
//  Created by Kirill Gorbushko on 27.03.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageCacheManager : NSObject

+ (instancetype)sharedManager;

- (void)setCacheImage:(UIImage *)image forKey:(NSString *)key;
- (UIImage *)getImageFromCacheForKey:(NSString *)key;
- (void)clearAllCache;

@end
