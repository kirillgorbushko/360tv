//
//  ImageCacheManager.m
//  360cam
//
//  Created by Kirill Gorbushko on 27.03.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ImageCacheManager.h"

@interface ImageCacheManager()

@property (strong, nonatomic) NSCache *imageCache;

@end

@implementation ImageCacheManager

#pragma mark - Public

+ (instancetype)sharedManager
{
    static ImageCacheManager *sharedManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[ImageCacheManager alloc] init];
    });
    return sharedManager;
}

- (void)setCacheImage:(UIImage *)image forKey:(NSString *)key
{
    [self.imageCache setObject:image forKey:key];
}

- (UIImage *)getImageFromCacheForKey:(NSString *)key
{
    UIImage *cachedImage;
    if ([self.imageCache objectForKey:key]) {
        cachedImage = [self.imageCache objectForKey:key];
    } else {
        cachedImage = nil;
    }
    return cachedImage;
}

- (void)clearAllCache
{
    [self.imageCache removeAllObjects];
}

#pragma mark - LifeCycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self prepareImageCache];
    }
    return self;
}

#pragma mark - Private

- (void)prepareImageCache
{
    self.imageCache = [[NSCache alloc] init];
    self.imageCache.totalCostLimit = 10 * 1024 *1024;
    self.imageCache.countLimit = 100;
    [self.imageCache setName:@"imagePreviewCache"];
}

@end
