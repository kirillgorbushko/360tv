#import "GiropticAnnotationView.h"

@implementation GiropticAnnotationView

- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Set the frame size to the appropriate values.
        CGRect  myFrame = self.frame;
        myFrame.size.width = 28;
        myFrame.size.height = 40;
        self.frame = myFrame;
        
        self.image = [UIImage imageNamed:@"ic-location-on.png"];
        
        self.draggable = YES;
        self.canShowCallout=YES;

        // The opaque property is YES by default. Setting it to
        // NO allows map content to show through any unrendered parts of your view.
        self.opaque = NO;
    }
    return self;
}
@end
