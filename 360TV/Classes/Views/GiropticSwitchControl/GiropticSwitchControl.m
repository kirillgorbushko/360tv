//
//  GiropticSwitchControl.m
//  GiropticSwitchControl
//
//  Created by Stas Volskyi on 5/18/15.
//  Copyright (c) 2015 Stas Volskyi. All rights reserved.
//

#import "GiropticSwitchControl.h"

static CGFloat const Height = 30;
static CGFloat const Width = 50;

@interface GiropticSwitchControl ()

@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIView *dotView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dotRightConstraint;

@end

@implementation GiropticSwitchControl

#pragma mark - Lifecycle

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self loadView];
        [self setupDefaults];
    }
    return self;
}

#pragma mark - Properties

- (BOOL)isOn
{
    return self.on;
}

#pragma mark - UI

- (void)loadView
{
    CGRect frame = self.frame;
    frame.size = CGSizeMake(Width, Height);
    self.frame = frame;
    NSString *nibName = NSStringFromClass([self class]);
    UIView *view = [[[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil] firstObject];
    view.frame = self.bounds;
    view.autoresizingMask = UIViewAutoresizingNone;
    [self addSubview:view];
}

- (void)setupDefaults
{
    self.dotView.layer.cornerRadius = CGRectGetHeight(self.dotView.frame) / 2;
    self.dotView.layer.borderWidth = 2.0;
    
    if (self.isOn) {
        [self fillOnState];
    } else {
        [self fillOffState];
    }
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [self addGestureRecognizer:tapGesture];
    
    UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGesture:)];
    swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [self addGestureRecognizer:swipeLeftGesture];
    
    UISwipeGestureRecognizer *swipeRightGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeGesture:)];
    swipeRightGesture.direction = UISwipeGestureRecognizerDirectionRight;
    [self addGestureRecognizer:swipeRightGesture];
}

#pragma mark - Gestures

- (void)tapGesture:(UIPanGestureRecognizer *)gesture
{
    if (self.isOn) {
        [self turnOff];
    } else {
        [self turnOn];
    }
}

- (void)swipeGesture:(UISwipeGestureRecognizer *)gesture
{
    switch (gesture.direction) {
        case UISwipeGestureRecognizerDirectionRight:
            [self turnOn];
            break;
        default:
            [self turnOff];
            break;
    }
}

#pragma mark - Switching

- (void)turnOn
{
    self.on = YES;
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.2 animations:^{
        weakSelf.dotView.backgroundColor = [UIColor colorWithRed:68.0/255.0 green:187.0/255.0 blue:255.0/255.0 alpha:1.0];
        weakSelf.dotView.layer.borderColor = [UIColor colorWithRed:68.0/255.0 green:187.0/255.0 blue:255.0/255.0 alpha:1.0].CGColor;
        weakSelf.lineView.backgroundColor = [UIColor colorWithRed:176.0/255.0 green:227.0/255.0 blue:255.0/255.0 alpha:1.0];
        weakSelf.dotRightConstraint.constant = 0;
        [weakSelf layoutIfNeeded];
    }];
    
    if ([self.delegate respondsToSelector:@selector(valueChanged:)]) {
        [self.delegate valueChanged:self];
    }
}

- (void)turnOff
{
    self.on = NO;
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.2 animations:^{
        weakSelf.dotView.backgroundColor = [UIColor whiteColor];
        weakSelf.dotView.layer.borderColor = [UIColor colorWithRed:145.0/255.0 green:145.0/255.0 blue:145.0/255.0 alpha:1.0].CGColor;
        weakSelf.lineView.backgroundColor = [UIColor colorWithRed:233.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1.0];
        weakSelf.dotRightConstraint.constant = CGRectGetWidth(self.bounds) - CGRectGetWidth(self.dotView.bounds);
        [weakSelf layoutIfNeeded];
    }];
    
    if ([self.delegate respondsToSelector:@selector(valueChanged:)]) {
        [self.delegate valueChanged:self];
    }
}

#pragma mark - Layout

- (void)fillOnState
{
    self.dotView.backgroundColor = [UIColor colorWithRed:68.0/255.0 green:187.0/255.0 blue:255.0/255.0 alpha:1.0];
    self.dotView.layer.borderColor = [UIColor colorWithRed:68.0/255.0 green:187.0/255.0 blue:255.0/255.0 alpha:1.0].CGColor;
    self.lineView.backgroundColor = [UIColor colorWithRed:176.0/255.0 green:227.0/255.0 blue:255.0/255.0 alpha:1.0];
    self.dotRightConstraint.constant = 0;
}

- (void)fillOffState
{
    self.dotView.backgroundColor = [UIColor whiteColor];
    self.dotView.layer.borderColor = [UIColor colorWithRed:145.0/255.0 green:145.0/255.0 blue:145.0/255.0 alpha:1.0].CGColor;
    self.lineView.backgroundColor = [UIColor colorWithRed:233.0/255.0 green:233.0/255.0 blue:233.0/255.0 alpha:1.0];
    self.dotRightConstraint.constant = CGRectGetWidth(self.bounds) - CGRectGetWidth(self.dotView.bounds);
}

@end
