//
//  GiropticSwitchControl.h
//  GiropticSwitchControl
//
//  Created by Stas Volskyi on 5/18/15.
//  Copyright (c) 2015 Stas Volskyi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SwitchControlDelegate <NSObject>

@optional
- (void)valueChanged:(id)sender;

@end

@interface GiropticSwitchControl : UIView

@property (assign, nonatomic) IBInspectable BOOL on;
@property (weak, nonatomic) id<SwitchControlDelegate> delegate;

- (BOOL)isOn;

@end
