#import "AFJSONAnyResponseSerializer.h"

@implementation AFJSONAnyResponseSerializer

- (id)init
{
    self = [super init];
    if (self) {
        self.acceptableContentTypes = [self.acceptableContentTypes setByAddingObject:@"text/plain"];
    }
    
    return self;
}

@end
