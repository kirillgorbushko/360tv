@interface JDSideMenu : UIViewController

@property (strong, nonatomic) UIViewController *tempContainer;

@property (nonatomic, readonly) UIViewController *contentController;
@property (nonatomic, readonly) UIViewController *menuController;

@property (nonatomic, assign) BOOL tapGestureEnabled;
@property (nonatomic, assign) BOOL panGestureEnabled;
@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;
@property (nonatomic, strong) UIPanGestureRecognizer *panRecognizer;

- (id)initWithContentController:(UIViewController*)contentController menuController:(UIViewController*)menuController;

- (void)setContentController:(UIViewController*)contentController animated:(BOOL)animated;

- (void)showMenuAnimated:(BOOL)animated;
- (void)hideMenuAnimated:(BOOL)animated;
- (BOOL)isMenuVisible;

- (void)setBackgroundImage:(UIImage*)image;

@end
