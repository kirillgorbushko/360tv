#import "JDSideMenu.h"
#import "GiropticHomeVC.h"
#import "GiropticSettingsVC.h"
#import "GiropticMediaPreviewVC.h"
#import "UIColor+AppDefaultColors.h"
#import "Animation.h"

const CGFloat JDSideMenuMinimumRelativePanDistanceToOpen = 0.0;
const CGFloat JDSideMenuDefaultDamping = 0.5;
const CGFloat JDSideMenuDimmedViewTag = 1001;
const CGFloat JDSideMenuDefaultOpenAnimationTime = 0.7;
const CGFloat JDSideMenuDefaultCloseAnimationTime = 0.2;

@interface JDSideMenu ()

@property (nonatomic, strong) UIImageView *backgroundView;
@property (nonatomic, strong) UIView *containerView;
@property (assign, nonatomic) CGFloat sideMenuWidth;

@end

@implementation JDSideMenu

- (id)initWithContentController:(UIViewController*)contentController menuController:(UIViewController*)menuController;
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        _contentController = contentController;
        _menuController = menuController;
        self.sideMenuWidth = self.view.frame.size.width * 0.85;
        _tapGestureEnabled = YES;
        _panGestureEnabled = YES;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (isIPad()) {
        [self prepareNotification];
    }
    
    self.sideMenuWidth = self.view.frame.size.width * 0.85;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if (isIPad()) {
        [self removeNotifications];
    }
}

#pragma mark - Notifications

- (void)prepareNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)removeNotifications
{
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    } @catch (NSException *exception) {
        NSLog(@"Cant remove observers %@", exception.debugDescription);
    }
}

- (void)orientationChanged:(NSNotification *)notification
{
    self.contentController.view.frame = self.view.frame;
    UIView *dimmedView = [self.containerView viewWithTag:JDSideMenuDimmedViewTag];
    dimmedView.frame = self.view.frame;
}

#pragma mark UIViewController

- (void)viewDidLoad;
{
    [super viewDidLoad];
    
    if (!isIPad()) {
        if (self.interfaceOrientation != UIInterfaceOrientationPortrait) {
            [[UIDevice currentDevice] performSelector:@selector(setOrientation:) withObject:(__bridge id)((void*)UIInterfaceOrientationPortrait)];
        }
    }
    
    [self addChildViewController:self.menuController];
    [self.menuController didMoveToParentViewController:self];
    [self addChildViewController:self.contentController];
    [self.contentController didMoveToParentViewController:self];
    
    _containerView = [[UIView alloc] initWithFrame:self.view.bounds];
    _containerView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
    [self.containerView addSubview:self.contentController.view];
    self.contentController.view.frame = self.containerView.bounds;
    self.contentController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_containerView];
    
    self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognized:)];
    self.tapRecognizer.enabled = NO;
    self.panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panRecognized:)];
//    [self.containerView addGestureRecognizer:self.tapRecognizer];
//    [self.containerView addGestureRecognizer:self.panRecognizer];
}

- (void)setBackgroundImage:(UIImage*)image;
{
    if (!self.backgroundView && image) {
        self.backgroundView = [[UIImageView alloc] initWithImage:image];
        self.backgroundView.frame = self.view.bounds;
        self.backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.view insertSubview:self.backgroundView atIndex:0];
    } else if (image == nil) {
        [self.backgroundView removeFromSuperview];
        self.backgroundView = nil;
    } else {
        self.backgroundView.image = image;
    }
}

#pragma mark controller replacement

- (void)setContentController:(UIViewController*)contentController animated:(BOOL)animated;
{
    [self addMenuControllerView];
    
    if (contentController == nil) return;
    UIViewController *previousController = self.contentController;
    _contentController = contentController;
    [self addChildViewController:self.contentController];
    
    self.contentController.view.frame = self.containerView.bounds;
    self.contentController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    __weak typeof(self) blockSelf = self;
    CGFloat offset = isIPad() ? self.sideMenuWidth * 1.5 : self.sideMenuWidth;
    [UIView animateWithDuration:JDSideMenuDefaultCloseAnimationTime/2.0 animations:^{
        blockSelf.containerView.transform = CGAffineTransformMakeTranslation(offset, 0);
      //  [blockSelf statusBarView].transform = blockSelf.containerView.transform;
    } completion:^(BOOL finished) {
        [blockSelf.containerView addSubview:self.contentController.view];
        [blockSelf.contentController didMoveToParentViewController:blockSelf];
        
        [previousController willMoveToParentViewController:nil];
        [previousController removeFromParentViewController];
        [previousController.view removeFromSuperview];
        
        [blockSelf hideMenuAnimated:YES];
    }];
}

#pragma mark Animation

- (void)tapRecognized:(UITapGestureRecognizer*)recognizer
{
    if (!self.tapGestureEnabled) return;
    
    if (![self isMenuVisible]) {
        [self showMenuAnimated:YES];
    } else {
        [self hideMenuAnimated:YES];
    }
}

- (void)panRecognized:(UIPanGestureRecognizer*)recognizer
{
    if (!self.panGestureEnabled) return;
    
    CGPoint translation = [recognizer translationInView:recognizer.view];
    CGPoint velocity = [recognizer velocityInView:recognizer.view];
    
    CGPoint touchPoint = [recognizer locationInView: recognizer.view];
    if (self.panGestureEnabled && touchPoint.x < 100)
    {
        switch (recognizer.state) {
            case UIGestureRecognizerStateBegan: {
                [self addMenuControllerView];
                [recognizer setTranslation:CGPointMake(recognizer.view.frame.origin.x, 0) inView:recognizer.view];
                break;
            }
            case UIGestureRecognizerStateChanged: {
                [recognizer.view setTransform:CGAffineTransformMakeTranslation(MAX(0,translation.x), 0)];
             //    [self statusBarView].transform = recognizer.view.transform;
                break;
            }
            case UIGestureRecognizerStateEnded:
            case UIGestureRecognizerStateCancelled: {
                if (velocity.x > 5.0 || (velocity.x >= -1.0 && translation.x > JDSideMenuMinimumRelativePanDistanceToOpen * self.sideMenuWidth)) {
                    CGFloat transformedVelocity = velocity.x/ABS(self.sideMenuWidth - translation.x);
                    CGFloat duration = JDSideMenuDefaultOpenAnimationTime * 0.66;
                    [self showMenuAnimated:YES duration:duration initialVelocity:transformedVelocity];
                } else {
                    [self hideMenuAnimated:YES];
                }
            }
            default:
                break;
        }
    }
}

- (void)addMenuControllerView;
{
    if (self.menuController.view.superview == nil) {
        CGRect menuFrame, restFrame;
        CGRectDivide(self.view.bounds, &menuFrame, &restFrame, self.sideMenuWidth, CGRectMinXEdge);
        self.menuController.view.frame = menuFrame;
        self.menuController.view.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
        self.view.backgroundColor = self.menuController.view.backgroundColor;
        if (self.backgroundView) [self.view insertSubview:self.menuController.view aboveSubview:self.backgroundView];
        else [self.view insertSubview:self.menuController.view atIndex:0];
    }
}

- (void)showMenuAnimated:(BOOL)animated;
{
    [self showMenuAnimated:animated duration:JDSideMenuDefaultOpenAnimationTime
           initialVelocity:1.0];
    
    UIView *dimmedView = [[UIView alloc] initWithFrame:self.contentController.view.frame];
    dimmedView.tag = JDSideMenuDimmedViewTag;
    [self.containerView addSubview:dimmedView];
    [dimmedView.layer addAnimation:[Animation fadeAnimFromValue:0. to:1.0 delegate:nil] forKey:nil];
    dimmedView.backgroundColor = [UIColor defaultTransparentBarsBackgroundColor];
}

- (void)showMenuAnimated:(BOOL)animated duration:(CGFloat)duration initialVelocity:(CGFloat)velocity;
{
    [self addMenuControllerView];
    
    __weak typeof(self) blockSelf = self;
    [UIView animateWithDuration:animated ? duration : 0.0 delay:0
         usingSpringWithDamping:JDSideMenuDefaultDamping initialSpringVelocity:velocity options:UIViewAnimationOptionAllowUserInteraction animations:^{
             blockSelf.containerView.transform = CGAffineTransformMakeTranslation(self.sideMenuWidth, 0);
           //  [self statusBarView].transform = blockSelf.containerView.transform;
         } completion:nil];
}

- (void)hideMenuAnimated:(BOOL)animated;
{
    UIView *dimmedView = [self.containerView viewWithTag:JDSideMenuDimmedViewTag];
    [dimmedView.layer addAnimation:[Animation fadeAnimFromValue:1.0 to:0. delegate:self] forKey:@"fadeAnim"];
    dimmedView.layer.opacity = 0.;
    
    self.contentController.view.frame = self.view.frame;
    
    __weak typeof(self) blockSelf = self;
    [UIView animateWithDuration:JDSideMenuDefaultCloseAnimationTime animations:^{
        blockSelf.containerView.transform = CGAffineTransformIdentity;
       // [self statusBarView].transform = blockSelf.containerView.transform;
    } completion:^(BOOL finished) {
        [blockSelf.menuController.view removeFromSuperview];
    }];
}

#pragma mark - AnimationDelegate

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    UIView *dimmedView = [self.containerView viewWithTag:JDSideMenuDimmedViewTag];

    if (anim == [dimmedView.layer animationForKey:@"fadeAnim"]) {
        [dimmedView removeFromSuperview];
    }
}

#pragma mark State

- (BOOL)isMenuVisible;
{
    return !CGAffineTransformEqualToTransform(self.containerView.transform, CGAffineTransformIdentity);
}

#pragma mark Statusbar

- (UIView*)statusBarView
{
    UIView *statusBar = nil;
    NSData *data = [NSData dataWithBytes:(unsigned char []){0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x42, 0x61, 0x72} length:9];
    NSString *key = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    id object = [UIApplication sharedApplication];
    if ([object respondsToSelector:NSSelectorFromString(key)]) statusBar = [object valueForKey:key];
    return statusBar;
}

@end
