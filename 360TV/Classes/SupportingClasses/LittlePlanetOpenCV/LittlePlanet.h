#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/photo/photo.hpp>
#include <cmath>
#include <thread>
#include <ctime>
 
using namespace std;
using namespace cv;


struct maStructure {

	Mat pixX;
	Mat pixY;
	double w = 0;
	double h = 0;
	double z = 0;
	double rads = 0;
	Mat xe;
};

struct maStructure2 {

	Mat pixX;
	Mat pixY;
	double w = 0;
	double h = 0;
	double z = 0;
	double rads = 0;
	Mat ye;
};

double d(double i, double j);

double r(double x, double y, double w, double h);

double rho(double x, double y, double w, double h, double z);

double theta(double x, double y, double w, double h, double z);

double a(double x, double y, double w, double h);

void thread_pixX(Mat *A);

void thread_pixY(Mat *A);

void thread_xe(maStructure *S);

void thread_ye(maStructure2 *S);

Mat LittlePlanet(Mat image);