#include "LittlePlanet.h"

double d(double i, double j)
{
	return i - j / 2;
}

double r(double x, double y, double w, double h)
{
	return sqrt(d(x, w)*d(x, w) + d(y, h)*d(y, h));
}

double rho(double x, double y, double w, double h, double z)
{
	return (r(x, y, w, h) / z*1.0);
}

double theta(double x, double y, double w, double h, double z)
{
	return 2 * atan(rho(x, y, w, h, z));
}

double a(double x, double y, double w, double h)
{
	return atan2(d(y, h), d(x, w));
}


void thread_pixX(Mat *A)
{
	for (int i = 0; i < (*A).rows; i++)
	{
		for (int j = 0; j < (*A).cols; j++)
		{

			(*A).at<float>(i, j) = j + 1;


		}


	}

}

void thread_pixY(Mat *A)
{
	for (int i = 0; i < (*A).rows; i++)
	{
		for (int j = 0; j < (*A).cols; j++)
		{

			(*A).at<float>(i, j) = i + 1;

		}

	}

}

void thread_xe(maStructure *S)
{
	Mat lon = Mat::zeros((*S).h, (*S).w, CV_32F);

	for (int i = 0; i < (*S).h; i++)
	{
		for (int j = 0; j < (*S).w; j++)
		{
			(*S).xe.at<float>(i, j) = (*S).w / 2.0 - ((fmod(a((*S).pixX.at<float>(i, j), (*S).pixY.at<float>(i, j), (*S).w, (*S).h) - M_PI / 4.0 + M_PI, M_PI*2.0) - M_PI) / (*S).rads);

		}

	}
}

void thread_ye(maStructure2 *S)
{
	Mat lat = Mat::zeros((*S).h, (*S).w, CV_32F);

	for (int i = 0; i < (*S).h; i++)
	{
		for (int j = 0; j < (*S).w; j++)
		{
			(*S).ye.at<float>(i, j) = (*S).h / 2.0 - ((fmod(theta((*S).pixX.at<float>(i, j), (*S).pixY.at<float>(i, j), (*S).w, (*S).h, (*S).z) + M_PI, M_PI) - M_PI*0.5) / (*S).rads);
		}

	}

}

/* function that makes the Little Planet. It takes one image in entry and it creates a Little Planet image. */
Mat LittlePlanet(Mat image)
{

	Mat src = image;
    
    //cvtColor(src,src, cv::COLOR_RGB2BGR);//CV_RGB2BGR);
    
	double w = src.cols;
	double h = src.rows;

	double CAMDIST = 6;
	double z = w / CAMDIST;
	double rads = 2.0 * M_PI / (w*1.0);


	Mat pixX = Mat::zeros(h, w, CV_32F);
	Mat pixY = Mat::zeros(h, w, CV_32F);
	Mat ye = Mat::zeros(h, w, CV_32F);
	Mat xe = Mat::zeros(h, w, CV_32F);

	std::thread T(thread_pixX, &pixX);
	std::thread T2(thread_pixY, &pixY);

	T.join();
	T2.join();

	maStructure S;
	S.pixX = pixX;
	S.pixY = pixY;
	S.h = h;
	S.w = w;
	S.z = z;
	S.rads = rads;
	S.xe = xe;

	maStructure2 S2;
	S2.pixX = pixX;
	S2.pixY = pixY;
	S2.h = h;
	S2.w = w;
	S2.z = z;
	S2.rads = rads;
	S2.ye = ye;

	std::thread T3(thread_xe, &S);
	std::thread T4(thread_ye, &S2);

	T3.join();
	T4.join();

	xe = S.xe;
	ye = S2.ye;

	Mat output;

//	remap(src, output, xe, ye, CV_INTER_CUBIC, BORDER_WRAP);
    remap(src, output, xe, ye, INTER_CUBIC, BORDER_WRAP);


	cv::Mat tmp;
	cv::GaussianBlur(output, tmp, cv::Size(5, 5), 5);
	cv::addWeighted(output, 1.5, tmp, -0.5, 0, output);
    
  //  cvtColor(src,src, cv::COLOR_BGR2RGB);// CV_BGR2RGB);
    
	return output;
}