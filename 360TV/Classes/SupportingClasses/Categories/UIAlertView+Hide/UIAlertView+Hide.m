//
//  UIAlertView+Hide.m
//  360cam
//
//  Created by Kirill Gorbushko on 14.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "UIAlertView+Hide.h"

@implementation UIAlertView (Hide)

- (void)hideAlert
{
    [self dismissWithClickedButtonIndex:[self cancelButtonIndex] animated:NO];
}

+ (void)hideAllAlerts
{
    for (UIWindow* window in [UIApplication sharedApplication].windows) {
        NSArray* subviews = window.subviews;
        if (subviews.count)
            if ([subviews[0] isKindOfClass:[UIAlertView class]])
                [(UIAlertView *)subviews[0] dismissWithClickedButtonIndex:[(UIAlertView *)subviews[0] cancelButtonIndex] animated:NO];
    }
}

@end
