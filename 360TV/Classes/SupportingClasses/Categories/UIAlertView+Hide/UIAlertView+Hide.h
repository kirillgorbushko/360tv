//
//  UIAlertView+Hide.h
//  360cam
//
//  Created by Kirill Gorbushko on 14.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertView (Hide)

- (void)hideAlert;
+ (void)hideAllAlerts;

@end
