//
//  NSString+Addition.h
//  testCamera360Api
//
//  Created by Kirill Gorbushko on 20.03.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Addition)

- (NSString *)stringByRemovingControlCharacters;

@end
