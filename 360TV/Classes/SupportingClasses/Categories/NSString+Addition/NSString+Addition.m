//
//  NSString+Addition.m
//  testCamera360Api
//
//  Created by Kirill Gorbushko on 20.03.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "NSString+Addition.h"

@implementation NSString (Addition)

- (NSString *)stringByRemovingControlCharacters
{
    NSCharacterSet *controlChars = [NSCharacterSet controlCharacterSet];
    NSRange range = [self rangeOfCharacterFromSet:controlChars];
    if (range.location != NSNotFound) {
        NSMutableString *mutable = [NSMutableString stringWithString:self];
        while (range.location != NSNotFound) {
            [mutable deleteCharactersInRange:range];
            range = [mutable rangeOfCharacterFromSet:controlChars];
        }
        return mutable;
    }
    return self;
}

@end
