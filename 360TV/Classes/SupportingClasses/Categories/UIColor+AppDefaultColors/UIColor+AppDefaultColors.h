//
//  UIColor+AppDefaultColors.h
//  iRemember
//
//  Created by Mykhailo Glagola on 02.03.15.
//  Copyright (c) 2015 ThinkMobiles. All rights reserved.
//



@interface UIColor (AppDefaultColors)

+ (UIColor *)defaultGrayColor;

+ (UIColor *)defaultMainTintColor;
+ (UIColor *)defaultHightlightedMainTintColor;
+ (UIColor *)defaultBlueTintColor;
+ (UIColor *)defaultRegularFontColor;
+ (UIColor *)defaultBarsBackgroundColor;
+ (UIColor *)defaultTransparentBarsBackgroundColor;
+ (UIColor *)defaultNavigationTitleColor;
+ (UIColor *)defaultBlueColor;
+ (UIColor *)defaultTransparentBlueColor;

+ (UIColor *)defaultTextColor;

@end
