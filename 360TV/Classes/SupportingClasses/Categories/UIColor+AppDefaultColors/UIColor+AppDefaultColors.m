//
//  UIColor+AppDefaultColors.m
//  iRemember
//
//  Created by Mykhailo Glagola on 02.03.15.
//  Copyright (c) 2015 ThinkMobiles. All rights reserved.
//

#import "UIColor+AppDefaultColors.h"

@implementation UIColor (AppDefaultColors)

#pragma mark - Public

+ (UIColor *)defaultGrayColor
{
    return [UIColor colorWithRealRed:184 green:184 blue:184 alpha:255];
}

+ (UIColor *)defaultMainTintColor
{
    return [UIColor colorWithRealRed:69 green:187 blue:255 alpha:255];
}

+ (UIColor *)defaultHightlightedMainTintColor
{
    return [UIColor colorWithRealRed:35 green:95 blue:123 alpha:255];
}

+ (UIColor *)defaultBlueTintColor
{
    return [UIColor colorWithRealRed:68 green:187 blue:255 alpha:255];
}

+ (UIColor *)defaultBarsBackgroundColor
{
    return [UIColor colorWithRealRed:119 green:119 blue:119 alpha:255];
}

+ (UIColor *)defaultRegularFontColor
{
    return [UIColor grayColor];
}

+ (UIColor *)defaultNavigationTitleColor
{
    return [UIColor blackColor];
}

+ (UIColor *)defaultTextColor
{
    return [UIColor colorWithRealRed:65 green:64 blue:66 alpha:255];
}

+ (UIColor *)defaultBlueColor
{
    return [UIColor colorWithRealRed:68 green:187 blue:254 alpha:255];
}

+ (UIColor *)defaultTransparentBlueColor
{
    return [UIColor colorWithRealRed:68 green:187 blue:254 alpha:0.9];
}

+ (UIColor *)defaultTransparentBarsBackgroundColor
{
    return [UIColor colorWithRealRed:0 green:0 blue:0 alpha:0.6];
}

#pragma mark - Private

+ (UIColor *)colorWithRealRed:(int)red green:(int)green blue:(int)blue alpha:(float)alpha
{
    return [UIColor colorWithRed:((float)red)/255.f green:((float)green)/255.f blue:((float)blue)/255.f alpha:((float)alpha)/1.f];
}

@end