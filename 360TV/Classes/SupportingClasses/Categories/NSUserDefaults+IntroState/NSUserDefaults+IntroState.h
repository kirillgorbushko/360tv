#import <Foundation/Foundation.h>

@interface NSUserDefaults (IntroState)

- (BOOL)introWasShown;
- (void)setIntroWasShown;

@end
