#import "NSUserDefaults+IntroState.h"

const NSString *introKey = @"IntroWasShown";

@implementation NSUserDefaults (IntroState)

- (BOOL)introWasShown
{
    return [self boolForKey:(NSString *)introKey];
}

- (void)setIntroWasShown
{
    [self setBool:YES forKey:(NSString *)introKey];
    [self synchronize];
}

@end
