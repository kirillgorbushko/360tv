//
//  AVAssestsLibraryManager.m
//  360TV
//
//  Created by Marin Todorov on 10/26/11.
//  Modified by Kirill Gorbushko on 26.03.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

typedef void(^Assets)(NSArray *assetsInAlbum);
typedef void(^AssetImageProcessingCompletitionBlock)(NSError* error, BOOL success);

@interface ALAssetsLibrary(CustomPhotoAlbum)

- (void)saveImage:(UIImage*)image withImageName:(NSString *)imageName albumName:(NSString *)albumName withCompletionBlock:(AssetImageProcessingCompletitionBlock)completionBlock;
- (void)addAssetURL:(NSURL*)assetURL toAlbum:(NSString*)albumName withCompletionBlock:(AssetImageProcessingCompletitionBlock)completionBlock;
- (void)getListOfFilesForPhotoAlbumName:(NSString *)photAlbumName withCompletionBlock:(Assets)completionBlock;

@end