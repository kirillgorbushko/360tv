//
//  AVAssestsLibraryManager.m
//  360TV
//
//  Created by Marin Todorov on 10/26/11.
//  Modified by Kirill Gorbushko on 26.03.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import <ImageIO/ImageIO.h>

@implementation ALAssetsLibrary(CustomPhotoAlbum)

#pragma mark - Public

- (void)saveImage:(UIImage*)image withImageName:(NSString *)imageName albumName:(NSString *)albumName withCompletionBlock:(AssetImageProcessingCompletitionBlock)completionBlock
{
    NSMutableDictionary *tiffMetadata = [[NSMutableDictionary alloc] init];
    [tiffMetadata setObject:@"Made with Giroptic 360 Cam" forKey:(NSString*)kCGImagePropertyTIFFImageDescription];
    [tiffMetadata setObject:imageName forKeyedSubscript:(NSString *)kCGImagePropertyTIFFDocumentName];
    
    NSMutableDictionary *metadata = [[NSMutableDictionary alloc] init];
    [metadata setObject:tiffMetadata forKey:(NSString*)kCGImagePropertyTIFFDictionary];
    
    [self writeImageToSavedPhotosAlbum:image.CGImage metadata:metadata completionBlock:^(NSURL *assetURL, NSError *error) {
        if (error) {
            completionBlock(error, NO);
        }
        [self addAssetURL:assetURL toAlbum:albumName withCompletionBlock:completionBlock];
    }];
}

- (void)addAssetURL:(NSURL*)assetURL toAlbum:(NSString*)albumName withCompletionBlock:(AssetImageProcessingCompletitionBlock)completionBlock
{
    __block BOOL albumWasFound = NO;
    __weak ALAssetsLibrary* weakSelf = self;

    [self enumerateGroupsWithTypes:ALAssetsGroupAlbum usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if ([albumName compare: [group valueForProperty:ALAssetsGroupPropertyName]]==NSOrderedSame) {
            albumWasFound = YES;
            [weakSelf assetForURL: assetURL resultBlock:^(ALAsset *asset) {
                [group addAsset: asset];
                NSLog(@"Photo saved to local album - %@", albumName);
                completionBlock(nil, YES);
            } failureBlock:^(NSError *error) {
                completionBlock(error, NO);
            }];
        }
        
        if (group && !albumWasFound) {
            [weakSelf addAssetsGroupAlbumWithName:albumName resultBlock:^(ALAssetsGroup *group) {
                [weakSelf assetForURL: assetURL resultBlock:^(ALAsset *asset) {
                    [group addAsset: asset];
                    NSLog(@"Asset added to group");
                    completionBlock(nil, YES);
                } failureBlock:^(NSError *error) {
                    completionBlock(error, NO);
                }];
            } failureBlock:^(NSError *error) {
                completionBlock(error, NO);
            }];
        }
    }  failureBlock:^(NSError *error) {
        completionBlock(error, NO);
    }];
}

- (void)getListOfFilesForPhotoAlbumName:(NSString *)photAlbumName withCompletionBlock:(Assets)completionBlock
{
    [self enumerateGroupsWithTypes:ALAssetsGroupAlbum usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if ([photAlbumName compare: [group valueForProperty:ALAssetsGroupPropertyName]]==NSOrderedSame) {
            __block NSMutableArray *assets = [[NSMutableArray alloc] init];

            [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
                if (result) {
                    [assets addObject:result];
                } else {
                    completionBlock(assets);
                    *stop = YES;
                }
            }];
            
        }
    } failureBlock:^(NSError *error) {
        completionBlock (nil);
    }];
}

@end