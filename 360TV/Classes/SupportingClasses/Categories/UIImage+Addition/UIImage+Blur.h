#import <Foundation/Foundation.h>

@interface UIImage (Blur)

- (UIImage *)blurredImageWithRadius:(CGFloat)radius;

@end
