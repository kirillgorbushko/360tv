#import <Foundation/Foundation.h>

@interface UIImage (Pixel)

+ (UIImage *)pixelImageWithColor:(UIColor *)color;

@end
