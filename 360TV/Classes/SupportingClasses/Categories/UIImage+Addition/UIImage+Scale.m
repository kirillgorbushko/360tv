#import "UIImage+Scale.h"

@implementation UIImage (Scale)

+ (UIImage *)imageWithData:(NSData *)data size:(CGSize)size fills:(BOOL)fills
{
    CFDataRef dataRef = CFDataCreateWithBytesNoCopy(NULL, data.bytes, data.length, kCFAllocatorNull);
    
    CGImageSourceRef imageSource = CGImageSourceCreateWithData(dataRef, NULL);
    
    CFDictionaryRef imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, NULL);
    if (imageProperties == NULL) {
        CFRelease(imageSource);
        CFRelease(dataRef);
        return nil;
    }
    
    CFNumberRef pixelWidthRef = CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelWidth);
    CFNumberRef pixelHeightRef = CFDictionaryGetValue(imageProperties, kCGImagePropertyPixelHeight);
    CGFloat width = [(__bridge NSNumber *)pixelWidthRef floatValue];
    CGFloat height = [(__bridge NSNumber *)pixelHeightRef floatValue];
    CFRelease(imageProperties);
    
    CGFloat widthScale = size.width / width;
    CGFloat heightScale = size.height / height;
    
    CGFloat minScale = MIN(widthScale, heightScale);
    
    UIImage *smallImage = nil;
    
    if (minScale <= 0.5 || !fills) {
        CGFloat maxSide;
        if (fills) {
            maxSide = widthScale > heightScale ? size.height : size.width;
        } else {
            maxSide = widthScale < heightScale ? size.height : size.width;
        }
        
        NSDictionary *thumbnailOptions = @{
                                           (id)kCGImageSourceCreateThumbnailWithTransform: (id)kCFBooleanTrue,
                                           (id)kCGImageSourceCreateThumbnailFromImageAlways: (id)kCFBooleanTrue,
                                           (id)kCGImageSourceThumbnailMaxPixelSize: @(maxSide)
                                           };
        
        CGImageRef smallImageRef = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, (__bridge CFDictionaryRef)thumbnailOptions);
        CFRelease(imageSource);
        CFRelease(dataRef);
        
        smallImage = [[UIImage alloc] initWithCGImage:smallImageRef];
        CGImageRelease(smallImageRef);
    } else {
        smallImage = [[UIImage alloc] initWithData:data];
    }

    return smallImage;
}

+ (UIImage *)imageWithData:(NSData *)data thatFills:(CGSize)size
{
    return [UIImage imageWithData:data size:size fills:YES];
}

+ (UIImage *)imageWithData:(NSData *)data thatFits:(CGSize)size
{
    return [UIImage imageWithData:data size:size fills:NO];
}

@end
