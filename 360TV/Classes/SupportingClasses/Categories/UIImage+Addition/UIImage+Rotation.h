//
//  UIImage+Rotation.h
//  360cam
//
//  Created by Kirill Gorbushko on 05.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Rotation)

+ (UIImage *)rotateImage:(UIImage *)image onDegrees:(float)degrees;

@end
