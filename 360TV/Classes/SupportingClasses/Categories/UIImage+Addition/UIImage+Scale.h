#import <Foundation/Foundation.h>
#import <ImageIO/ImageIO.h>

@interface UIImage (Scale)

+ (UIImage *)imageWithData:(NSData *)data thatFills:(CGSize)size;
+ (UIImage *)imageWithData:(NSData *)data thatFits:(CGSize)size;

@end
