//
//  UIImage+Rotation.m
//  360cam
//
//  Created by Kirill Gorbushko on 05.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "UIImage+Rotation.h"

@implementation UIImage (Rotation)

+ (UIImage *)rotateImage:(UIImage *)image onDegrees:(float)degrees
{
    CGFloat rads = M_PI * degrees / 180;
    UIGraphicsBeginImageContext(image.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(ctx, image.size.width, image.size.height);
    CGContextRotateCTM(ctx, rads);
    CGContextDrawImage(UIGraphicsGetCurrentContext(),CGRectMake(0.,0., image.size.width, image.size.height),image.CGImage);
    UIImage *i = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return i;
}


@end
