

#import "TYMActivityIndicatorView.h"

@interface UIWindow (UIActivityIndicator)

-(void)showIndicatorWithText:(NSString *)message;
-(void)hideIndicator;

@end