
#import "UIWindow+UIActivityIndicator.h"
#import "UIColor+AppDefaultColors.h"

@implementation UIWindow (UIActivityIndicator)

-(void)showIndicatorWithText:(NSString *)message
{
    CGRect screenRect = (isIPad()) ? self.frame : [[UIScreen mainScreen] bounds];
    
    
    UIView *whiteBoxView = [[UIView alloc] initWithFrame:CGRectMake( (screenRect.size.width-250)/2, (screenRect.size.height-175)/2, 250, 175)];
    whiteBoxView.tag = 100;
    whiteBoxView.backgroundColor = [UIColor whiteColor];
    whiteBoxView.layer.cornerRadius = 5;
    whiteBoxView.clipsToBounds = NO;
    whiteBoxView.layer.shadowOffset = CGSizeMake(0, 3);
    whiteBoxView.layer.shadowColor = [UIColor colorWithRed:119.0/255.0 green:119.0/255.0 blue:119.0/255.0 alpha:1.0].CGColor;
    whiteBoxView.layer.shadowOpacity = 0.8;

    
    TYMActivityIndicatorView *activityIndicatorView2 = [[TYMActivityIndicatorView alloc] initWithActivityIndicatorStyle:TYMActivityIndicatorViewStyleLarge];
    activityIndicatorView2.hidesWhenStopped = NO;
    activityIndicatorView2.tag = 100;

    UIView *loaderBGAlphaView;
    if (isIPad()) {
        loaderBGAlphaView = [[UIView alloc] initWithFrame:self.frame];
    } else {
        loaderBGAlphaView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    }
    loaderBGAlphaView.tag = 100;
    loaderBGAlphaView.backgroundColor = [UIColor blackColor];
    loaderBGAlphaView.alpha = 0.5;

    [self addSubview:loaderBGAlphaView];
    [self addSubview:whiteBoxView];
    [self addSubview:activityIndicatorView2];

    whiteBoxView.center = loaderBGAlphaView.center;
    activityIndicatorView2.center = loaderBGAlphaView.center;
    [activityIndicatorView2 startAnimating];
    
    CGRect loadingFrame = CGRectMake(activityIndicatorView2.frame.origin.x-50, whiteBoxView.frame.origin.y + 10, activityIndicatorView2.frame.size.width+100, 24);
    UILabel *loadingLbl = [[UILabel alloc] initWithFrame:loadingFrame];
    loadingLbl.tag = 100;
    loadingLbl.textAlignment = NSTextAlignmentCenter;
//    loadingLbl.font = RobotoMediumFontWithSize(16);
    loadingLbl.text = @"Please wait...";
    loadingLbl.textColor = [UIColor defaultRegularFontColor];
    loadingLbl.backgroundColor = [UIColor clearColor];
    [self addSubview:loadingLbl];

    CGRect messageFrame = CGRectMake(0, whiteBoxView.frame.origin.y + whiteBoxView.frame.size.height - 34, loaderBGAlphaView.frame.size.width, 24);
    UILabel *messageLbl = [[UILabel alloc] initWithFrame:messageFrame];
    messageLbl.tag = 100;
    messageLbl.textAlignment = NSTextAlignmentCenter;
//    messageLbl.font = RobotoMediumFontWithSize(16);
    messageLbl.text = message;
    messageLbl.textColor = [UIColor grayColor];
    messageLbl.backgroundColor = [UIColor clearColor];
    [self addSubview:messageLbl];

}

-(void)hideIndicator
{
    for (UIView *view in self.subviews) {
        if (view.tag == 100) {
            [view removeFromSuperview];
        }
    }
}

@end
