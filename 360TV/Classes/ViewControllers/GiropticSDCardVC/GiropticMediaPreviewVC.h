#import <UIKit/UIKit.h>
#import "DetailViewManager.h"

typedef NS_ENUM(NSUInteger, ControllerViewMode) {
    ControllerViewModeNone,
    ControllerViewModeSDCard,
    ControllerViewModeCameraRoll
};

@interface GiropticMediaPreviewVC : UIViewController <SubstitutableDetailViewController>

@property (assign, nonatomic) ControllerViewMode photoViewMode;
@property (copy, nonatomic) NSString *filesPath;

@end