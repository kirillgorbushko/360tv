#import "GiropticMediaPreviewVC.h"
#import "UIViewController+JDSideMenu.h"
#import "GiropticMediaListTableCell.h"
#import "SVProgressHUD.h"
#import "CameraNetworkManager.h"
#import "GiropticMediaFile.h"
#import "GiropticEndPoints.h"
#import "ImageCacheManager.h"
#import "UIImage+Scale.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "UIColor+AppDefaultColors.h"
#import "GiropticPhotoPreviewVC.h"
#import "UIImage+Rotation.h"
#import "UIImage+OpenCV.h"
#import "NSString+Addition.h"

#import <MediaPlayer/MediaPlayer.h>

static NSInteger const CellHeight = 160;
static NSInteger const TralingSpaceForButtons = 50;
static NSString *const CellIdentifier = @"MediaTableViewCell";
static NSString *const StorageMode = @"Storage";
static NSString *const PreviewMode = @"Preview";

@interface GiropticMediaPreviewVC () <UITableViewDataSource, UITableViewDelegate, GiropticMediaListTableCellDelegate, UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIView *topMenuContainer;
@property (weak, nonatomic) IBOutlet UITableView *mediaTblView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *informationLabel;
@property (strong, nonatomic) IBOutlet UIView *parentView;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingSpaceMenuButtonConstraint;

@property (nonatomic, strong) NSMutableArray *mediaFilesArray;
@property (assign, nonatomic) NSInteger invalidEntriesCount;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (assign, nonatomic) __block BOOL isUpdatingProcessInProgress;
@property (strong, nonatomic) ALAssetsLibrary  *assetsLibrary;
@property (strong, nonatomic) UIRefreshControl *bottomRefreshControl;

@property (strong, nonatomic) GiropticMediaListTableCell *selectedCell;
@property (copy, nonatomic) __block NSString *continuationToken;

@end

@implementation GiropticMediaPreviewVC

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self forceDeviceOrientation];
    [self prepareUI];
    [self prepareLocalVariables];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupLeftButton];
    [self setupBottomRefreshControl];
        
    if (!self.mediaFilesArray.count) {
        [[CameraNetworkManager sharedManager] setStartPointForFileListToZero];
        [self configureDataSource];
    }
    
    if (isIPad() && [[CameraNetworkManager sharedManager] isCameraConnected]) {
        self.leadingSpaceMenuButtonConstraint.constant = 16;
        self.menuButton.hidden = YES;
    }
    
#warning TO Test method
//    [[CameraNetworkManager sharedManager] giropticGetFullListOfFilesForPath:@"/mnt/mmc/DCIM/360CAM" operationResult:^(BOOL success, id response, NSError *error) {
//        
//    }];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [[ImageCacheManager sharedManager] clearAllCache];
    self.mediaTblView.bottomRefreshControl = nil;
}

#pragma mark - Rotation

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - IBActions

- (IBAction)leftMenuTapped:(id)sender
{
    if (self.filesPath) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self.sideMenuController showMenuAnimated:YES];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.mediaFilesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GiropticMediaListTableCell *cell = (GiropticMediaListTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[GiropticMediaListTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (self.photoViewMode == ControllerViewModeSDCard) {
        GiropticMediaFile *file = self.mediaFilesArray[indexPath.row];
        cell.fileNameLbl.text = file.fileName;
        
        [cell prepareUIWith:file];
        
        if (file.fileType == FileTypeJPG || file.fileType == FileTypeBURST || file.fileType == FileTypeTIMELAPS) {
            cell.downloadBtn.hidden = NO;
            cell.tralingInfoConstraint.constant = TralingSpaceForButtons;
        }
        [self setImageFromSDForCell:cell withItem:file forIndexPath:indexPath];
    } else if (self.photoViewMode == ControllerViewModeCameraRoll) {
        [self setImageForCell:cell indexPath:indexPath];
    }
    cell.delegate = self;
    
    return cell;
}

- (void)setImageFromSDForCell:(GiropticMediaListTableCell *)cell withItem:(GiropticMediaFile *)file forIndexPath:(NSIndexPath *)indexPath
{
    void (^CompleteDownloadImagePreview)(UIImage *imagePreview, NSString *key, NSIndexPath *indexPath) = ^(UIImage *imagePreview, NSString *key, NSIndexPath *indexPath) {
        dispatch_async(dispatch_get_main_queue(), ^{
           GiropticMediaListTableCell *selectedCell = (GiropticMediaListTableCell *)[self.mediaTblView cellForRowAtIndexPath:indexPath];
            selectedCell.mediaImgView.image = imagePreview;
            [[ImageCacheManager sharedManager] setCacheImage:imagePreview forKey:key];
        });
    };
    
    UIImage *image = [[ImageCacheManager sharedManager] getImageFromCacheForKey:[GiropticMediaFile getKeyForFile:file.fileName suffix:PreviewMode]];
    NSString *key = [GiropticMediaFile getKeyForFile:file.fileName suffix:PreviewMode];
    
    if (image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.mediaImgView.image = image;
        });
    } else {
        if (file.fileType < 4) {
            [[CameraNetworkManager sharedManager] giropticGetImagePreview:[self prepareFilePathForGiropticFile:file] imageWidth:cell.mediaImgView.frame.size.width imageHeight:cell.mediaImgView.frame.size.height operationResult:^(BOOL success, id response, NSError *error) {
                if (success) {
                    if ([[response valueForKey:@"type"] isEqualToString:@"base64"]) {
                        NSData *data =[[NSData alloc] initWithBase64EncodedString:[response valueForKey:@"data"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
                        UIImage *image = [UIImage imageWithData:data];
                        if (image) {
                            CompleteDownloadImagePreview(image, key, indexPath);
                        }
                    } else {
                        NSLog(@"error while try to get file - %@, %@", file.fileName, error.localizedDescription);
                    }
                }
                [SVProgressHUD dismiss];
            }];
        } else if (file.fileType == FileTypeVIDEO) {
            cell.previewLabel.hidden = NO;
            //tbd
        }
    }
}

- (NSString *)prepareFilePathForGiropticFile:(GiropticMediaFile *)file
{
    NSString *filePath;
    
    switch (file.fileType) {
        case FileTypeBURST:
        case FileTypeHDR:
        case FileTypeTIMELAPS: {
            filePath = [NSString stringWithFormat:@"%@/%@/%@", GiroImageFilePath, file.fileName, PhotoFileDefaultName];
            break;
        }
        case FileTypeJPG: {
            filePath = [NSString stringWithFormat:@"%@/%@", GiroImageFilePath, file.fileName];
        }
        default: {
            break;
        }
    }
    return filePath;
}

- (void)setImageForCell:(GiropticMediaListTableCell *)cell indexPath:(NSIndexPath *)cellIndexPath
{
    ALAsset *selectedAsset = self.mediaFilesArray[cellIndexPath.row];
    
    [cell prepareUIWithALAsset:selectedAsset];
    cell.fileNameLbl.text = selectedAsset.defaultRepresentation.filename;
    
    UIImage *image = [[ImageCacheManager sharedManager] getImageFromCacheForKey:[GiropticMediaFile getKeyForFile:selectedAsset.defaultRepresentation.filename suffix:StorageMode]];
    NSString *key = [GiropticMediaFile getKeyForFile:selectedAsset.defaultRepresentation.filename suffix:StorageMode];
    
    if (image) {
        cell.mediaImgView.image = image;
    } else {
        [self.assetsLibrary assetForURL:selectedAsset.defaultRepresentation.url resultBlock:^(ALAsset *asset) {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage *cellImage = [UIImage imageWithCGImage:asset.aspectRatioThumbnail];
                cell.mediaImgView.image = cellImage;
                [[ImageCacheManager sharedManager] setCacheImage:cellImage forKey:key];
            });
        } failureBlock:^(NSError *error) {
            NSLog(@"Cant get image - %@", selectedAsset.defaultRepresentation.filename);
        }];
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.photoViewMode == ControllerViewModeSDCard) {
        GiropticMediaFile *media = [self.mediaFilesArray objectAtIndex:indexPath.row];
        if (media.fileType == FileTypeJPG || media.fileType == FileTypeVIDEO) {
            [SVProgressHUD setBackgroundColor:[UIColor defaultTransparentBlueColor]];
            [SVProgressHUD showWithStatus:@"Preparing media asset for preview" maskType:SVProgressHUDMaskTypeGradient];
            [self downloadForViewMediaFile:media];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Not implemented" message:@"This feature not implemented" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
//            [self showViewControllerWithMode:ControllerViewModeSDCard filesPath:[NSString stringWithFormat:@"%@/%@/", GiroImageFilePath, media.fileName]];
        }
    } else if (self.photoViewMode == ControllerViewModeCameraRoll) {
        ALAsset *selectedImageAsset = self.mediaFilesArray[indexPath.row];
        UIImage *selectedImage = [UIImage imageWithCGImage:selectedImageAsset.defaultRepresentation.fullResolutionImage];
        UIImage *jpegImage = [UIImage imageWithData:UIImageJPEGRepresentation(selectedImage, 1.0)];
        [self showGiropticViewControllerWithImage:jpegImage withName:selectedImageAsset.defaultRepresentation.filename];
    }
}

- (void)downloadForViewMediaFile:(GiropticMediaFile *)mediaFile
{
    __weak GiropticMediaPreviewVC *weakSelf = self;
    [[CameraNetworkManager sharedManager] giropticGetImage:[mediaFile.fileURL path] imageWidth:0 imageHeight:0 operationResult:^(BOOL success, id response, NSError *error) {
        if (success) {
            [weakSelf showGiropticViewControllerWithImage:response withName:[[mediaFile.fileURL path] lastPathComponent]];
        } else {
            NSLog(@"error while try to get file - %@", [mediaFile.fileURL path]);
        }
    }];
}

#pragma mark - GiropticMediaListTableCellDelegate

- (void)shareButtonDidTappedForCell:(GiropticMediaListTableCell *)cell
{
    NSLog(@"Share");
    //tbd
}

- (void)downloadButtonDidTappedForCell:(GiropticMediaListTableCell *)cell
{
    GiropticMediaFile *media = [self.mediaFilesArray objectAtIndex:[self.mediaTblView indexPathForCell:cell].row];

    if (media.fileType == FileTypeBURST || media.fileType == FileTypeTIMELAPS) {
        [[[UIAlertView alloc] initWithTitle:@"Not implemented" message:@"This feature not implemented" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    } else if (media.fileType == FileTypeJPG) {
        self.selectedCell = cell;
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Save image to camera roll" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Normal", @"Little planet", nil];
        [actionSheet showInView:self.view];
    }
}

- (void)infoButtonDidTappedForCell:(GiropticMediaListTableCell *)cell
{
    NSLog(@"info");
    //tbd
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0: {
            [self saveGiroImageToCameraRoll];
            break;
        }
        case 1: {
            [self saveGiroLittlePlanetToCameraRoll];
            break;
        }
        default:
            break;
    }
}

#pragma mark - Navigation

- (void)showViewControllerWithMode:(ControllerViewMode)controllerMode filesPath:(NSString *)filesPath
{
    GiropticMediaPreviewVC *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"GiropticMediaPreviewVC"];
    viewController.photoViewMode = controllerMode;
    viewController.filesPath = filesPath;
    
    if (isIPad()) {
        UINavigationController *navc = [[UINavigationController alloc] initWithRootViewController:viewController];
        navc.navigationBarHidden = YES;
        
        DetailViewManager *detailViewManager = (DetailViewManager*)self.splitViewController.delegate;
        UIViewController <SubstitutableDetailViewController> *detailViewController = nil;
        
        detailViewController = (id) navc;
        detailViewManager.detailViewController = detailViewController;
    } else {
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

#pragma mark - Private

- (void)saveGiroImageToCameraRoll
{
    void (^SaveImageToCameraRoll)(UIImage *image, NSString *filePath) = ^(UIImage *image, NSString *filePath) {
        [self.assetsLibrary saveImage:image withImageName:[filePath lastPathComponent] albumName:AlbumNameGiropticLocalFies  withCompletionBlock:^(NSError *error, BOOL success) {
            if (success) {
                [SVProgressHUD showSuccessWithStatus:@"Image saved to camera roll"];
            } else {
                [SVProgressHUD showErrorWithStatus:@"Cant save image"];
            }
        }];
    };
    
    [self getImageFromCameraCompletion:^(BOOL done, UIImage *image, NSString *filePath) {
        if (done) {
            SaveImageToCameraRoll(image, filePath);
        } else {
            [SVProgressHUD showErrorWithStatus:@"Cant get image from camera"];
        }
    }];
}

- (void)saveGiroLittlePlanetToCameraRoll
{
    void (^SaveLittlePlanetImage)(UIImage *image, NSString *filePath) = ^(UIImage *image, NSString *filePath) {
        ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
        [assetsLibrary saveImage:image withImageName:[NSString stringWithFormat:@"Giro Little Planet - %@", [filePath lastPathComponent]] albumName:AlbumNameGiropticLittlePlanetLocalFies withCompletionBlock:^(NSError *error, BOOL success) {
            if (success) {
                [SVProgressHUD showSuccessWithStatus:@"Little planet image saved"];
            } else {
                [SVProgressHUD showErrorWithStatus:@"Cant save image"];
            }
        }];
    };
    
    [self getImageFromCameraCompletion:^(BOOL done, UIImage *image, NSString *filePath) {
        if (done) {
            UIImage* flippedImage = [UIImage rotateImage:image onDegrees:180];
            Mat modImageMat = LittlePlanet([UIImage cvMatFromUIImage:flippedImage]);
            UIImage *littlePlanetSnapshot = [UIImage UIImageFromCVMat:modImageMat];
            SaveLittlePlanetImage(littlePlanetSnapshot, filePath);
        } else {
            [SVProgressHUD showErrorWithStatus:@"Cant get image from camera"];
        }
    }];
}

- (void)getImageFromCameraCompletion:(void(^)(BOOL done, UIImage *image, NSString *filePath))completitionHandler
{
    [SVProgressHUD setBackgroundColor:[UIColor defaultTransparentBlueColor]];
    [SVProgressHUD showWithStatus:@"Saving image..." maskType:SVProgressHUDMaskTypeGradient];
    
    NSIndexPath *selectedImageIndexPath = [self.mediaTblView indexPathForCell:self.selectedCell];
    NSString *filePath = [((GiropticMediaFile *)self.mediaFilesArray[selectedImageIndexPath.row]).fileURL path];
    
    [[CameraNetworkManager sharedManager] giropticGetImage:filePath imageWidth:0 imageHeight:0 operationResult:^(BOOL success, id response, NSError *error) {
        if (success) {
            completitionHandler(YES, response, filePath);
        } else {
            completitionHandler(NO, nil, nil);
        }
    }];
}

- (void)showGiropticViewControllerWithImage:(UIImage *)imageToShow withName:(NSString *)fileName
{
    [SVProgressHUD dismiss];

    GiropticPhotoPreviewVC *photoController = [self.storyboard instantiateViewControllerWithIdentifier:@"detailedPhotoView"];
    photoController.sourceImage = imageToShow;
    photoController.imageName = fileName;
    UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    photoController.sphereWindow = window;
    window.rootViewController = [[UINavigationController alloc] initWithRootViewController:photoController];
    [window makeKeyAndVisible];
}

- (void)getLocalGiropticFiles
{
    __weak GiropticMediaPreviewVC *weakSelf = self;
    [self.assetsLibrary getListOfFilesForPhotoAlbumName:AlbumNameGiropticLocalFies withCompletionBlock:^(NSArray *assetsInAlbum) {
        if (assetsInAlbum.count) {
            [weakSelf prepareDataSourceWithAVAssets:assetsInAlbum];
        } else {
            [weakSelf showNoFilesStoredMessage];
        }
    }];
}

- (void)prepareDataSourceWithAVAssets:(NSArray *)assetsArray
{
    [self.mediaFilesArray removeAllObjects];
    self.mediaFilesArray = [[NSArray arrayWithArray:assetsArray] mutableCopy];
    [self.mediaTblView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationBottom];
}

- (void)showNoFilesStoredMessage
{
    self.informationLabel.hidden = NO;
    self.mediaTblView.hidden = YES;
}

- (void)parseDirectoriesList:(NSArray *)filesFromResponse
{
    for (int i = 0; i < filesFromResponse.count; i++) {
        NSString *fileName = ((GiropticMediaFile *)filesFromResponse[i]).fileName;
        if (!([fileName hasPrefix:@"."] || [fileName hasPrefix:@"_"])) {
            [self.mediaFilesArray addObject:filesFromResponse[i]];
            [self.mediaTblView beginUpdates];
             [self.mediaTblView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:self.mediaFilesArray.count - 1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.mediaTblView endUpdates];
        } else {
            self.invalidEntriesCount++;
        }
    }
    self.isUpdatingProcessInProgress = NO;
}

- (void)fetchNewPhotosPortion
{
    if ((self.mediaFilesArray.count + self.invalidEntriesCount) < [CameraNetworkManager sharedManager].totalEntriesCount) {
        [self getListOfSDFiles:^{
            if (self.photoViewMode == ControllerViewModeSDCard) {
                [self.bottomRefreshControl endRefreshing];
            }
        }];
    }
}

- (void)configureDataSource
{
    if (self.photoViewMode == ControllerViewModeSDCard) {
        [self getListOfSDFiles:nil];
    } else {
        [self getLocalGiropticFiles];
    }
}

- (void)getListOfSDFiles:(void(^)())completitionHandler
{
    if (self.photoViewMode == ControllerViewModeSDCard) {
        self.isUpdatingProcessInProgress = YES;
        __weak GiropticMediaPreviewVC *weakSelf = self;
        [[CameraNetworkManager sharedManager] giropticGetListOfFilesCount:10 filePath:self.filesPath continuationToken:self.continuationToken operationResult:^(BOOL success, id response, NSError *error) {
            if (success) {
                [weakSelf parseDirectoriesList:response];
                if (completitionHandler) {
                    completitionHandler();
                    weakSelf.continuationToken = [CameraNetworkManager sharedManager].continuationToken;
                }
            } else {
                NSLog(@"Cant get files from camera %@", error.localizedDescription);
                self.isUpdatingProcessInProgress = NO;
                [self.bottomRefreshControl endRefreshing];
            }
        }];
    }
}

- (void)setupBottomRefreshControl
{
    if (self.photoViewMode == ControllerViewModeSDCard) {
        self.bottomRefreshControl = [[UIRefreshControl alloc] init];
        self.bottomRefreshControl.tintColor = [UIColor defaultBlueColor];
        [self.bottomRefreshControl addTarget:self action:@selector(fetchNewPhotosPortion) forControlEvents:UIControlEventValueChanged];
        self.mediaTblView.bottomRefreshControl = self.bottomRefreshControl;
    }
}

- (void)prepareLocalVariables
{
    self.mediaFilesArray = [NSMutableArray array];
    self.assetsLibrary = [[ALAssetsLibrary alloc] init];
    self.continuationToken = @"";
}

- (void)prepareUI
{
    self.view.layer.borderWidth = 0.5;
    self.view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.mediaTblView.separatorColor = [UIColor clearColor];
    
    self.mediaTblView.contentOffset = CGPointZero;  
}

- (void)forceDeviceOrientation
{
    if (!isIPad()) {
        if (self.interfaceOrientation != UIInterfaceOrientationPortrait) {
            [[UIDevice currentDevice] performSelector:@selector(setOrientation:) withObject:(__bridge id)((void*)UIInterfaceOrientationPortrait)];
        }
    }
}

- (void)setupLeftButton
{
    if (self.filesPath) {
        [self.menuBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    } else {
        [self.menuBtn addGestureRecognizer:self.sideMenuController.tapRecognizer];
    }
}

@end