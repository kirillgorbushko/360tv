//
//  MainPreviewViewController.m
//  360cam
//
//  Created by Kirill Gorbushko on 03.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "MainPreviewViewController.h"
#import "ContentViewController.h"
#import "UIViewController+JDSideMenu.h"
#import "GiropticEndPoints.h"
#import "AppDelegate.h"
#import "Animation.h"
#import <MediaPlayer/MediaPlayer.h>

#import "SPHBaseViewController.h"
#import "SPHPhotoViewController.h"
#import "GiropticPhotoPreviewVC.h"

static NSString *const PhotoSegue = @"photo";
static NSString *const VideoSegue = @"video";

@interface MainPreviewViewController () <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *leftMenuButton;

@property (weak, nonatomic) IBOutlet UIView *portraitView;
@property (weak, nonatomic) IBOutlet UIView *landscapeView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingButtonTextConstraint;
@property (strong, nonatomic) NSArray *rawJSON;

@end

@implementation MainPreviewViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addMenuNavigationGestures];
    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self refreshInterfaceIfNeeded];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (isIPad()) {
        [self removeNotifications];
    }
}

#pragma mark - IBActions

- (IBAction)menuButtonPressed:(id)sender
{
    NSArray *viewControllers = ((UINavigationController *)self.sideMenuController.contentController).viewControllers;
    if ([viewControllers[0] isKindOfClass:[self class]]) {
        self.sideMenuController.tempContainer = viewControllers[0];
    }

    [self.sideMenuController showMenuAnimated:YES];
}

- (IBAction)viewIntroButtonPressed:(id)sender
{
    MPMoviePlayerViewController *moviewViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:PreviewPresentationURL]];
    [self presentMoviePlayerViewControllerAnimated:moviewViewController];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSPredicate *predicate;
    if ([segue.identifier isEqualToString:PhotoSegue]) {
        predicate = [NSPredicate predicateWithFormat:@"type LIKE 'photo'"];
    } else if ([segue.identifier isEqualToString:VideoSegue]){
        predicate = [NSPredicate predicateWithFormat:@"type LIKE 'video'"];
    }

    NSArray *content = [self.rawJSON filteredArrayUsingPredicate:predicate];
    if (content.count) {
        ((ContentViewController *)segue.destinationViewController).dataSource = content;
    } else {
        [[[UIAlertView alloc] initWithTitle:[AppDelegate appName] message:@"Server doesn't response." delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil] show];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Notifications

- (void)prepareNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)removeNotifications
{
    @try {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    } @catch (NSException *exception) {
        NSLog(@"Cant remove observers %@", exception.debugDescription);
    }
}

- (void)orientationChanged:(NSNotification *)notification
{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void)adjustViewsForOrientation:(UIInterfaceOrientation)orientation
{
    switch (orientation) {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown: {
            [self.landscapeView.layer addAnimation:[Animation fadeAnimFromValue:1. to:0. delegate:self] forKey:@"portrait"];
            self.portraitView.hidden = NO;
            [self.portraitView.layer addAnimation:[Animation fadeAnimFromValue:0. to:1. delegate:nil] forKey:nil];
            break;
        }
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight: {
            [self.portraitView.layer addAnimation:[Animation fadeAnimFromValue:1. to:0. delegate:self] forKey:@"landscape"];
            self.landscapeView.hidden = NO;
            [self.landscapeView.layer addAnimation:[Animation fadeAnimFromValue:0. to:1. delegate:nil] forKey:nil];
            break;
        }
        case UIInterfaceOrientationUnknown:{
            break;
        }
    }
}

#pragma mark - AnimationDelegate

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.landscapeView.layer animationForKey:@"portrait"]) {
        [self.landscapeView.layer removeAllAnimations];
        self.landscapeView.hidden = YES;

    } else if (anim == [self.portraitView.layer animationForKey:@"landscape"]) {
        [self.portraitView.layer removeAllAnimations];
        self.portraitView.hidden = YES;
    }
}

#pragma mark - Private

- (void)addMenuNavigationGestures
{
    if (self.sideMenuController.tapRecognizer) {
        [self.leftMenuButton addGestureRecognizer:self.sideMenuController.tapRecognizer];
    }
}

- (void)refreshInterfaceIfNeeded
{
    if (!isIPad()) {
        [[UIDevice currentDevice] setValue: [NSNumber numberWithInteger: UIInterfaceOrientationPortrait] forKey:@"orientation"];
    } else {
        if (isPortrait()) {
            self.landscapeView.hidden = YES;
            self.portraitView.hidden = NO;
        } else {
            self.portraitView.hidden = YES;
            self.landscapeView.hidden = NO;
        }
        [self prepareNotification];
    }
    self.leadingButtonTextConstraint.constant = self.view.frame.size.width * 0.22;
}

- (void)loadData
{
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    [sessionConfiguration setHTTPAdditionalHeaders:@{@"Content-Type": @"application/json", @"Accept": @"application/json"}];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:PreviewGiroAPI]];
    [request setHTTPMethod:@"GET"];
    
    __weak MainPreviewViewController *weakSelf = self;
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            weakSelf.rawJSON = (NSArray *)jsonResponse;
        }
    }];
    
    [dataTask resume];
}

@end
