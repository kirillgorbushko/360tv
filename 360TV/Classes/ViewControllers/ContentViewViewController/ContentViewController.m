//
//  ContentViewViewController.m
//  360cam
//
//  Created by Kirill Gorbushko on 03.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ContentViewController.h"
#import "ContentViewTableViewCell.h"
#import "SPHBaseViewController.h"
#import "GiropticEndPoints.h"
#import "GiropticPhotoPreviewVC.h"
#import "GiropticVideoPreviewVC.h"
#import "SVProgressHUD.h"
#import "NetworkManager.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "ContentViewCollectionViewCell.h"

static NSString *const TableViewCellIdentifier = @"contentViewCell";
static NSString *const CollectionViewCellIdentifier = @"previewCell";
static CGFloat const Offset = 45;

@interface ContentViewController () <UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *controllerTitle;

@property (strong, nonatomic) NSMutableDictionary *thumbnails;
@property (assign, nonatomic) MediaType mediaType;

@end

@implementation ContentViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self generateTitle];
    [self loadThumbnails];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [self updateScrollDirection];
}

#pragma mark - Rotation

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.2 animations:^{
        [weakSelf.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    }];
//    [self updateScrollDirection];
}

#pragma mark - IBActions

- (IBAction)pressingBackButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContentViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TableViewCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[ContentViewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TableViewCellIdentifier];
    }
    NSDictionary *dictionary = self.dataSource[indexPath.row];
    if (self.thumbnails[dictionary[@"thumb_path"]]) {
        cell.previewImageView.image = self.thumbnails[dictionary[@"thumb_path"]];
    }
    cell.previewLabel.text = dictionary[@"title"];

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContentViewTableViewCell *cell = (ContentViewTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
    [self didSelectItemAtIndexPath:indexPath];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ContentViewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CollectionViewCellIdentifier forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[ContentViewCollectionViewCell alloc] init];
    }
    NSDictionary *dictionary = self.dataSource[indexPath.row];
    if (self.thumbnails[dictionary[@"thumb_path"]]) {
        cell.previewImageView.image = self.thumbnails[dictionary[@"thumb_path"]];
    }
    cell.previewLabel.text = dictionary[@"title"];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ContentViewCollectionViewCell *cell = (ContentViewCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.selected = NO;
    [self didSelectItemAtIndexPath:indexPath];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize contentSize = self.collectionView.frame.size;
    CGSize cellSize;
    
    if (isPortrait()) {
        cellSize.height = (contentSize.height - Offset) / 3.5;
        cellSize.width = (contentSize.width - Offset) / 2;
    } else {
        cellSize.height = (contentSize.height - Offset) / 2.5;
        cellSize.width = (contentSize.width - Offset * 1.5) / 3;
    }
    
    return cellSize;
}

#pragma mark - Private

- (void)didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    if (networkStatus) {
        [self showContentAtIndex:indexPath];
    } else {
        [[[UIAlertView alloc] initWithTitle:[AppDelegate appName] message:@"Sorry, no internet avaliable. Please check your connection..." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

- (void)updateScrollDirection
{
    if (isIPad()) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
        
        if (isPortrait()) {
            [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
        } else {
            [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        }
        [self.collectionView setCollectionViewLayout:flowLayout];
        [self.collectionView.collectionViewLayout invalidateLayout];
    }
}

- (void)loadThumbnails
{
    self.thumbnails = [[NSMutableDictionary alloc] init];
    __weak ContentViewController *weakSelf = self;
    for (NSDictionary *dict in self.dataSource) {
        NSString *filePath = [NSString stringWithFormat:@"%@%@", PreviewGiroAPIBase, dict[@"thumb_path"]];
        dispatch_async(dispatch_queue_create("queue.loadingPreview.async", nil), ^{
            NSString *key = dict[@"thumb_path"];
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:filePath]]];
            if (image) {
                [weakSelf.thumbnails setObject:image forKey:key];
            }
            if ([weakSelf.thumbnails allKeys].count == self.dataSource.count) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf.tableView reloadData];
                    [weakSelf.collectionView reloadData];
                });
            }
        });
    }
}

- (void)generateTitle
{
    NSString *title = self.dataSource[0][@"type"];
    if ([title isEqualToString:@"video"]) {
        self.mediaType = MediaTypeVideo;
    } else {
        self.mediaType = MediaTypePhoto;
    }
    title = [title capitalizedString];
    self.controllerTitle.text = [NSString stringWithFormat:@"%@s", title ? title : @"No"];
}

- (void)showContentAtIndex:(NSIndexPath *)indexPath
{
    NSDictionary *dict = self.dataSource[indexPath.row];
    SPHBaseViewController *baseController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone" bundle:nil];
    
    switch (self.mediaType) {
        case MediaTypePhoto: {
            baseController = [storyboard instantiateViewControllerWithIdentifier:@"detailedPhotoView"];
            
            [SVProgressHUD showWithStatus:@"Preparing Image" maskType:SVProgressHUDMaskTypeGradient];
            [[NetworkManager sharedManager] giropticGetImageWithPath:[NSString stringWithFormat:@"%@%@", PreviewGiroAPIBase, dict[@"path_high"]] withCompletition:^(BOOL success, UIImage *image) {
                [SVProgressHUD dismiss];
                if (success) {
                    baseController.sourceImage = image;
                    UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                    ((GiropticPhotoPreviewVC *)baseController).sphereWindow = window;
                    window.rootViewController = [[UINavigationController alloc] initWithRootViewController:baseController];
                    [window makeKeyAndVisible];
                } else {
                    [[[UIAlertView alloc] initWithTitle:[AppDelegate appName] message:@"Cant get image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
                }
            }];
            break;
        }
        case MediaTypeVideo: {
            baseController = [storyboard instantiateViewControllerWithIdentifier:@"GiropticVideoPreview"];
            baseController.sourceVideoURL = [NSString stringWithFormat:@"http://player.vimeo.com/external/%@", dict[@"path_high"]];
            
            UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
            ((GiropticVideoPreviewVC *)baseController).sphereWindow = window;
            window.rootViewController = [[UINavigationController alloc] initWithRootViewController:baseController];
            [window makeKeyAndVisible];
            break;
        }

        default:
            break;
    }
}

@end
