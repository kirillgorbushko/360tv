#import "GiropticHomeVC.h"
#import "UIViewController+JDSideMenu.h"
#import "AppDelegate.h"
#import "UIWindow+UIActivityIndicator.h"
#import "GiropticMediaPreviewVC.h"
#import "SVProgressHUD.h"
#import "Animation.h"
#import "CameraNetworkManager.h"
#import "UIColor+AppDefaultColors.h"
#import "GiropticMediaFile.h"
#import "LocationManager.h"
#import "GiropticSettingsParameters.h"
#import "GiropticPhotoPreviewVC.h"
#import "GiropticEndPoints.h"
#import "GiropticVideoPreviewVC.h"
#import "GiropticSettingsVC.h"

static NSString *const GiropticVideoPreviewVCName = @"GiropticVideoPreview";

@interface GiropticHomeVC () <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *playerView;
@property (weak, nonatomic) IBOutlet UIView *settingsView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *modeSettingsLbl;
@property (weak, nonatomic) IBOutlet UILabel *fpsSettingsLbl;
@property (weak, nonatomic) IBOutlet UILabel *timerSettingsLbl;
@property (weak, nonatomic) IBOutlet UIView *fileNameContainer;
@property (weak, nonatomic) IBOutlet UILabel *fileNameLbl;
@property (weak, nonatomic) IBOutlet UIButton *leftMenuButton;
@property (weak, nonatomic) IBOutlet UIView *videoControlsContainer;
@property (weak, nonatomic) IBOutlet UILabel *recordingTimerLbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewPositionConstraint;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;

@property (weak, nonatomic) IBOutlet UIButton *videoModeButton;
@property (weak, nonatomic) IBOutlet UIButton *photoModeButton;
@property (weak, nonatomic) IBOutlet UIButton *burstModeButton;
@property (weak, nonatomic) IBOutlet UIButton *timeLapseModeButton;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UIImageView *gyroStatusImageView;
@property (weak, nonatomic) IBOutlet UIImageView *gpsStatusImageView;
@property (weak, nonatomic) IBOutlet UILabel *photoTimerLabel;
@property (weak, nonatomic) IBOutlet UILabel *previewLabel;
@property (assign, nonatomic) NSInteger timeBeforeSnap;

@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSTimer *internalTimer;

@property (copy, nonatomic) NSString *filePath;

@end

@implementation GiropticHomeVC

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[LocationManager sharedManager] permissionRequest];
    [self prepareUI];

    [self startProcess];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self addNetworkObserver];
    [self addTapGestures];
    [self getCurrentOptionFromCamera];

//    [self prepareContainerViewWithRTSP:
//     @"rtsp://media1.law.harvard.edu/Media/policy_a/2012/02/02_unger.mov"];
//     @"rtsp://184.72.239.149/vod/mp4:BigBuckBunny_175k.mov/"];
//    @"rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov"];
//    @"rtsp://192.168.1.168:8556/PSIA/Streaming/channels/2?videoCodecType=H.264"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self removeNotifications];
}

#pragma mark - IBActions

- (IBAction)videoModeButtonPress:(id)sender
{
    [self showIndicatorWithText:@"Switching to Video mode"];
    __weak GiropticHomeVC *weakSelf = self;
        
    [[CameraNetworkManager sharedManager] giropticSwitchToMode:GiropticCameraModeVideo operationResult:^(BOOL success, id response, NSError *error) {
        if (success) {
            [weakSelf setViewControllerTitle:@"Video Mode"];
        }
    }];
}

- (IBAction)photoModeButtonPress:(id)sender
{
    [self showIndicatorWithText:@"Switching to Photo mode"];
    __weak GiropticHomeVC *weakSelf = self;
    
    [[CameraNetworkManager sharedManager] giropticSwitchToMode:GiropticCameraModeImage operationResult:^(BOOL success, id response, NSError *error) {
        if (success) {
            [weakSelf setViewControllerTitle:@"Photo Mode"];
        }
    }];
}

- (IBAction)burstModePressed:(id)sender
{
    [self showIndicatorWithText:@"Switching to Burst mode"];
    __weak GiropticHomeVC *weakSelf = self;
    
    [[CameraNetworkManager sharedManager] giropticSwitchToMode:GiropticCameraModeBurst operationResult:^(BOOL success, id response, NSError *error) {
        if (success) {
            [weakSelf setViewControllerTitle:@"Burst Mode"];
        }
    }];
}

- (IBAction)timeLapseModeButtonPress:(id)sender
{
    [self showIndicatorWithText:@"Switching to TimeLapse mode"];
    __weak GiropticHomeVC *weakSelf = self;
    
    [[CameraNetworkManager sharedManager] giropticSwitchToMode:GiropticCameraModeTimeLaps operationResult:^(BOOL success, id response, NSError *error) {
        if (success) {
            [weakSelf setViewControllerTitle:@"TimeLapse Mode"];
        }
    }];
}

#warning - TO REMOVE - TEST RTSP 
- (IBAction)tistLiveModePressed:(id)sender
{
    [self showIndicatorWithText:@"Switching to Live mode"];
    __weak GiropticHomeVC *weakSelf = self;
    
    [[CameraNetworkManager sharedManager] giropticSwitchToMode:GiropticCameraModeLive operationResult:^(BOOL success, id response, NSError *error) {
        if (success) {
            [weakSelf setViewControllerTitle:@"Live Mode"];
            [weakSelf getStreamURLForPreview];
        }
    }];

}

- (IBAction)captureButtonPress:(id)sender
{
    __weak GiropticHomeVC *weakSelf = self;

    if ([UIImageJPEGRepresentation(self.recordButton.imageView.image, 1.0) isEqualToData:UIImageJPEGRepresentation([UIImage imageNamed:@"stop_video"], 1.0)]) {
        [[CameraNetworkManager sharedManager] giropticStopRecording:^(BOOL success, id response, NSError *error) {
            if (success) {
                [weakSelf updateCaptureButtonWithImage:@"Timelapse_mode"];
            }
        }];
        return;
    }
    
    self.recordButton.userInteractionEnabled = NO;
    [self shouldBlockUserInteractionWithUI:YES];
    if (!self.fileNameContainer.hidden) {
        [self.internalTimer invalidate];
        [self shouldShowBottomView:NO];
    }
    
    switch ([CameraNetworkManager sharedManager].selectedMode) {
        case GiropticCameraModeImage: {
            [self startPhotoTimer];
            
            [[CameraNetworkManager sharedManager] giropticTakePicture:^(BOOL success, id response, NSError *error) {
                if (success) {
                    weakSelf.filePath = [[response valueForKey:@"results"] valueForKey:@"fileUri"];
                    weakSelf.fileNameLbl.text = [weakSelf.filePath lastPathComponent];
                    [weakSelf shouldShowBottomView:YES];
                    weakSelf.internalTimer = [NSTimer scheduledTimerWithTimeInterval:5.f target:self selector:@selector(hidePhotoView) userInfo:nil repeats:NO];
                }
                weakSelf.recordButton.userInteractionEnabled = YES;
                [weakSelf shouldBlockUserInteractionWithUI:NO];
            }];
            break;
        }
        case GiropticCameraModeBurst: {
            [[CameraNetworkManager sharedManager] giropticTakePicture:^(BOOL success, id response, NSError *error) {
                if (success) {
                }
                weakSelf.recordButton.userInteractionEnabled = YES;
                [weakSelf shouldBlockUserInteractionWithUI:NO];
            }];
            break;
        }
        case GiropticCameraModeLive: {
            [[CameraNetworkManager sharedManager] giropticGetStreamURLWithType:GiropticStreamURLTypePreview operationResult:^(BOOL success, id response, NSError *error) {
                if (success) {
                    //todo
                }
            }];
            break;
        }
        case GiropticCameraModeTimeLaps: {
            [[CameraNetworkManager sharedManager] giropticStartRecording:^(BOOL success, id response, NSError *error) {
                if (success) {
                    [weakSelf updateCaptureButtonWithImage:@"stop_video"];
                    weakSelf.recordButton.userInteractionEnabled = YES;
                }
            }];
            break;
        }
        case GiropticCameraModeVideo: {
            [self shouldShowVideoRecordingControls:YES];
            [[CameraNetworkManager sharedManager] giropticStartRecording:^(BOOL success, id response, NSError *error) {
                if (success) {
                    [weakSelf startVideoRecordTimer];
                }
            }];
            break;
        }
        default:
            break;
    }
}

- (IBAction)stopRecordingTapped:(id)sender
{
    [self shouldShowVideoRecordingControls:NO];
    [self stopVideoRecordTimer];

    __weak GiropticHomeVC *weakSelf = self;
    [[CameraNetworkManager sharedManager] giropticStopRecording:^(BOOL success, id response, NSError *error) {
        if (success) {
            NSLog(@"Stoped successfuly");
        }
        weakSelf.recordButton.userInteractionEnabled = YES;
        [weakSelf shouldBlockUserInteractionWithUI:NO];
    }];
}

- (IBAction)viewFileTapped:(id)sender
{
    [SVProgressHUD showWithStatus:@"Preparing image" maskType:SVProgressHUDMaskTypeGradient];
    [[CameraNetworkManager sharedManager] giropticGetImage:self.filePath imageWidth:0 imageHeight:0 operationResult:^(BOOL success, id response, NSError *error) {
        [SVProgressHUD dismiss];
        if (success) {
            GiropticPhotoPreviewVC *photoController = [self.storyboard instantiateViewControllerWithIdentifier:@"detailedPhotoView"];
            photoController.sourceImage = response;
            photoController.imageName = [self.filePath lastPathComponent];
            UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
            photoController.sphereWindow = window;
            window.rootViewController = [[UINavigationController alloc] initWithRootViewController:photoController];
            [window makeKeyAndVisible];
        } else {
            NSLog(@"cant get file - %@", self.filePath);
        }
    }];
}

- (IBAction)modifyBtnTapped:(id)sender
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        NSArray *viewControllers = ((UINavigationController *)self.sideMenuController.contentController).viewControllers;
        if ([viewControllers[0] isKindOfClass:[GiropticHomeVC class]]) {
            self.sideMenuController.tempContainer = viewControllers[0];
        }
        UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"GiropticSettingsVC"];
        UINavigationController *navc = [[UINavigationController alloc] initWithRootViewController:viewController];
        navc.navigationBarHidden = YES;
        [self.sideMenuController setContentController:navc animated:NO];
    } else {
        GiropticSettingsVC *settings = [self.storyboard instantiateViewControllerWithIdentifier:@"GiropticSettingsVC"];
        settings.shownFromModify = YES;
        [self.navigationController pushViewController:settings animated:YES];
    }
}

- (IBAction)leftMenuBtnTapped:(id)sender
{
    [self.sideMenuController showMenuAnimated:YES];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex) {
        NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:settingsURL];
    } else {
        [self startProcess];
    }
}

#pragma mark - Private

#pragma mark - RecordingTimer

- (void)prepareContainerViewWithRTSP:(NSString *)rtspUri
{
    __weak typeof(self) weakSelf = self;
    dispatch_queue_t myQueue = dispatch_queue_create("com.thinkmobiles.giroptic.rtsp.queue", DISPATCH_QUEUE_SERIAL);
    dispatch_async(myQueue, ^{
        if (weakSelf.childViewControllers.count) {
            ((GiropticPhotoPreviewVC *)weakSelf.childViewControllers[0]).mode = PreviewModeRTSPPlayer;
            ((GiropticPhotoPreviewVC *)weakSelf.childViewControllers[0]).rtspUri = rtspUri;
        }
    });
}

- (void)startVideoRecordTimer
{
    self.startDate = [NSDate date];
    self.internalTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimerLabel) userInfo:nil repeats:YES];
    [self.internalTimer fire];
}

- (void)stopVideoRecordTimer
{
    [self.internalTimer invalidate];
    self.internalTimer = nil;
}

- (void)updateTimerLabel
{
    NSDate *currentDate = [NSDate date];
    NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:self.startDate];
    NSDate *timerDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0.0]];
    
    NSString *timeString = [dateFormatter stringFromDate:timerDate];
    self.recordingTimerLbl.text = timeString;
}

#pragma mark - PhotoTimer

- (void)startPhotoTimer
{
    self.timeBeforeSnap = [[[self.timerSettingsLbl.text componentsSeparatedByString:@" "] lastObject] integerValue];

    if (self.timeBeforeSnap) {
        self.internalTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updatePhotoLabel) userInfo:nil repeats:YES];
        [self setSecondsForLabel:self.timeBeforeSnap];
        self.photoTimerLabel.hidden = NO;
        self.previewLabel.hidden = YES;
    }
}

- (void)updatePhotoLabel
{
    [self setSecondsForLabel:self.timeBeforeSnap];
    if (self.timeBeforeSnap) {
        self.timeBeforeSnap--;
    } else {
        [self.internalTimer invalidate];
        self.photoTimerLabel.hidden = YES;
        self.previewLabel.hidden = NO;
    }
}

- (void)setSecondsForLabel:(NSInteger)seconds
{
    if (seconds > 9) {
        self.photoTimerLabel.text = [NSString stringWithFormat:@"00:%i", (int)seconds];
    } else {
        self.photoTimerLabel.text = [NSString stringWithFormat:@"00:0%i", (int)seconds];
    }
}

- (void)hidePhotoView
{
    [self.internalTimer invalidate];
    [self shouldShowBottomView:NO];
}

#pragma mark - CustomLoader

- (void)showIndicatorWithText:(NSString *)message
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.window showIndicatorWithText:message];
}

- (void)hideIndicator
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.window hideIndicator];
}

#pragma mark - APIUsage

- (void)startProcess
{    
    if (![CameraNetworkManager sharedManager].sessionID.length) {
        [self startSession];
    }
}

- (void)startSession
{
    [self showIndicatorWithText:@"Starting session"];

    void (^StartSessionBlock)() = ^() {

        __weak GiropticHomeVC *weakSelf = self;
        [[CameraNetworkManager sharedManager] giropticStartSessionWithDurationInSec:DefaultSessionDuration result:^(BOOL success, id response, NSError *error) {
            [weakSelf hideIndicator];
            if (success) {
                [weakSelf getCurrentOptionFromCamera];
               // [weakSelf getStreamURLForPreview];
                NSLog(@"Session started");
            } else {
                NSLog(@"Error occured during camera session starting");
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:GiroCurrentSessionID];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[[UIAlertView alloc] initWithTitle:[AppDelegate appName] message:@"Cant startSession. Please try again later or restart your camera." delegate:self cancelButtonTitle:@"Try again" otherButtonTitles: nil] show];
            }
        }];
    };
    
    NSString *prevSessionId;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:GiroCurrentSessionID]) {
        prevSessionId = [[NSUserDefaults standardUserDefaults] objectForKey:GiroCurrentSessionID];
    }
    if (prevSessionId.length) {
        NSLog(@"Try to clear previous session");
        [[CameraNetworkManager sharedManager] giropticCloseSessionWithSessionId:prevSessionId operationResult:^(BOOL success, id response, NSError *error) {
            if (success) {
                NSLog(@"Prev session closed");
            } else {
                NSLog(@"Cant clear prev session");
            }
            StartSessionBlock();
        }];
    } else {
        StartSessionBlock();
    }
}

- (void)getCurrentOptionFromCamera
{
    NSArray *requestedOptions = @[
                                  GiroOptionKeyCaptureMode,
                                  GiroOptionKeyGPS,
                                  GiroOptionKeyGYRO,
                                  GiroOptionKeyHDR,
                                  GiroOptionKeyRemainingSpace,
                                  GiroOptionKeyTimeLapsInterval,
                                  GiroOptionKeyVideoBitrate,
                                  GiroOptionKeyVideoFrameRate,
                                  GiroOptionKeyWhiteBalance,
                                  GiroOptionKeyTotalSpace,
                                  GiroOptionKeyFileFormat,
                                  GiroOptionKeyButteryLevel,
                                  GiroOptionKeyBurstDuration,
                                  GiroOptionKeyExplosureDelay,
                                  ];
    __block NSDictionary *responseWithOptions = [NSDictionary dictionary];
    __weak GiropticHomeVC *weakSelf = self;
    [[CameraNetworkManager sharedManager] giropticGetOptions:requestedOptions operationResult:^(BOOL success, id response, NSError *error) {
        if (success) {
            responseWithOptions = [[response valueForKey:@"results"] valueForKey:@"options"];
            [weakSelf displayCurrentSettingOnUI:responseWithOptions];
        }
    }];
}

- (void)setOptions
{
    __weak GiropticHomeVC *weakSelf = self;
    [[CameraNetworkManager sharedManager] giropticSetOptions:@{GiroOptionKeyVideoBitrate : @(500000)} operationResult:^(BOOL success, id response, NSError *error) {
        if (success) {
            [weakSelf getCurrentOptionFromCamera];
        }
    }];
}

- (void)getStreamURLForPreview
{
    __weak typeof(self) weakSelf = self;
    [[CameraNetworkManager sharedManager] giropticGetStreamURLWithType:GiropticStreamURLTypePreview operationResult:^(BOOL success, id response, NSError *error) {
        if (success) {
            NSString *streamUri = [[response valueForKey:@"results"] valueForKey:@"url"];
            NSLog(@"Sream url preview - %@", streamUri);
            [weakSelf prepareContainerViewWithRTSP:streamUri];
        }
    }];
}

#pragma mark - UI

- (void)setViewControllerTitle:(NSString *)newTitle
{
    [self hideIndicator];
    self.titleLbl.text = newTitle;
    [self getCurrentOptionFromCamera];
    [self updateUIForCurrentCameraMode];
}

- (void)displayCurrentSettingOnUI:(NSDictionary *)currentSetting
{
    self.modeSettingsLbl.text = [NSString stringWithFormat:@"Mode: %@", [self getCurrentModeDescriptionFromResponse:currentSetting]];
    if ([currentSetting valueForKey:@"_videoFramerate"]) {
        self.fpsSettingsLbl.text = [NSString stringWithFormat:@"FPS: %@", [currentSetting valueForKey:@"_videoFramerate"]];
    } else {
        self.fpsSettingsLbl.text = @"FPS: N/A for selected mode";
    }
    self.timerSettingsLbl.text = [NSString stringWithFormat:@"Exposure Delay: %@", [currentSetting valueForKey:@"exposureDelay"]];
    
    BOOL gpsEnabled = [[currentSetting valueForKey:@"gps"] integerValue];
    BOOL gyroEnabled = [[currentSetting valueForKey:@"gyro"] integerValue];
    
    self.gpsStatusImageView.image = gpsEnabled ? [UIImage imageNamed:@"gps"] : [UIImage imageNamed:@"gps_inactive"];
    self.gyroStatusImageView.image = gyroEnabled ? [UIImage imageNamed:@"Gyro_blue"] : [UIImage imageNamed:@"Gyro_blue_inactive"];
    
    [self updateUIForCurrentCameraMode];
}

- (NSString *)getCurrentModeDescriptionFromResponse:(NSDictionary *)currentSetting
{
    NSString *captureMode = [currentSetting valueForKey:@"captureMode"];
    NSString *mode;
    if ([captureMode isEqualToString:GiroCaptureModeImage]) {
        [CameraNetworkManager sharedManager].selectedMode = GiropticCameraModeImage;
        mode = @"Photo Mode";
    } else if ([captureMode isEqualToString:GiroCaptureModeVideo]) {
        [CameraNetworkManager sharedManager].selectedMode = GiropticCameraModeVideo;
        mode = @"Video Mode";
    } else if ([captureMode isEqualToString:GiroCaptureModeBurst]) {
        [CameraNetworkManager sharedManager].selectedMode = GiropticCameraModeBurst;
        mode = @"Burst Mode";
    } else if ([captureMode isEqualToString:GiroCaptureModeTimeLapse]) {
        [CameraNetworkManager sharedManager].selectedMode = GiropticCameraModeTimeLaps;
        mode = @"TimeLapse Mode";
    } else if ([captureMode isEqualToString:GiroCaptureModeLive]) {
        [CameraNetworkManager sharedManager].selectedMode = GiropticCameraModeLive;
        mode = @"Live Mode";
    }
    self.titleLbl.text = mode;

    return mode;
}

- (void)prepareUI
{ 
    self.view.layer.borderWidth = 0.5;
    self.view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.settingsView.layer.borderColor = [UIColor defaultGrayColor].CGColor;
    self.settingsView.layer.borderWidth = 0.3;
}

- (void)addTapGestures
{
    [self.leftMenuButton addGestureRecognizer:self.sideMenuController.tapRecognizer];
}

- (void)updateUIForCurrentCameraMode
{
    self.videoModeButton.selected = NO;
    self.photoModeButton.selected = NO;
    self.burstModeButton.selected = NO;
    self.timeLapseModeButton.selected = NO;
    
    switch ([CameraNetworkManager sharedManager].selectedMode) {
        case GiropticCameraModeImage: {
            self.photoModeButton.selected = YES;
            [self updateCaptureButtonWithImage:@"Photo_mode"];
            break;
        }
        case GiropticCameraModeBurst: {
            self.burstModeButton.selected = YES;
            [self updateCaptureButtonWithImage:@"Burst_mode"];
            break;
        }
        case GiropticCameraModeLive: {
            break;
        }
        case GiropticCameraModeTimeLaps: {
            self.timeLapseModeButton.selected = YES;
            [self updateCaptureButtonWithImage:@"Timelapse_mode"];
            break;
        }
        case GiropticCameraModeVideo: {
            self.videoModeButton.selected = YES;
            [self updateCaptureButtonWithImage:@"Video_mode"];
            break;
        }
        default:
            break;
    }
}

- (void)updateCaptureButtonWithImage:(NSString *)imageName
{
    [self.recordButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [self.recordButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateHighlighted];
    [self.recordButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateSelected];
}

- (void)shouldBlockUserInteractionWithUI:(BOOL)shouldBlockUserInteractions
{
    self.videoModeButton.userInteractionEnabled = !shouldBlockUserInteractions;
    self.photoModeButton.userInteractionEnabled = !shouldBlockUserInteractions;
    self.burstModeButton.userInteractionEnabled = !shouldBlockUserInteractions;
    self.timeLapseModeButton.userInteractionEnabled = !shouldBlockUserInteractions;
}

#pragma mark - Animations

- (void)shouldShowBottomView:(BOOL)shouldShow
{
    CABasicAnimation *stateAnimation;
    if (shouldShow) {
        stateAnimation = [Animation fadeAnimFromValue:0 to:1. delegate:nil];
        self.fileNameContainer.hidden = NO;
    } else {
        stateAnimation = [Animation fadeAnimFromValue:1. to:0 delegate:self];
    }
    
    CABasicAnimation *positionAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
    positionAnimation.fromValue = [NSValue valueWithCGPoint:self.fileNameContainer.center];
    CGPoint toPoint = CGPointMake(self.fileNameContainer.center.x, self.fileNameContainer.center.y);
    if (shouldShow) {
        toPoint.y -= self.fileNameContainer.bounds.size.height;
    } else {
        toPoint.y += self.fileNameContainer.bounds.size.height;
    }
    positionAnimation.toValue = [NSValue valueWithCGPoint:toPoint];
    positionAnimation.duration = 0.3;
    positionAnimation.repeatCount = 1;
    positionAnimation.removedOnCompletion = YES;
    positionAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.animations = @[positionAnimation, stateAnimation];
    [self.fileNameContainer.layer addAnimation:stateAnimation forKey:@"snackBarView"];
    self.fileNameContainer.layer.position = toPoint;
    
    if (shouldShow) {
        self.bottomViewPositionConstraint.constant = 0;
    }
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.fileNameContainer.layer animationForKey:@"snackBarView"]) {
        self.fileNameContainer.hidden = YES;
        [self.fileNameContainer.layer removeAllAnimations];
    }
}

- (void)shouldShowVideoRecordingControls:(BOOL)shouldShow
{
    CABasicAnimation *hideAnimation = [Animation fadeAnimFromValue:1. to:0 delegate:nil];
    CABasicAnimation *showAnimation = [Animation fadeAnimFromValue:0 to:1. delegate:nil];
    
    if (shouldShow) {
        self.videoControlsContainer.hidden = !shouldShow;
        [self.videoControlsContainer.layer addAnimation:showAnimation forKey:nil];
        [self.recordButton.layer addAnimation:hideAnimation forKey:nil];
        self.recordButton.hidden = shouldShow;
        [self startVideoRecordTimer];
    } else {
        self.recordButton.hidden = shouldShow;
        [self.recordButton.layer addAnimation:showAnimation forKey:nil];
        [self.videoControlsContainer.layer addAnimation:hideAnimation forKey:nil];
        self.videoControlsContainer.hidden = !shouldShow;
        [self stopVideoRecordTimer];
    }
}

#pragma mark - Notifications

- (void)addNetworkObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showErrorInNetwork) name:NO_INTERNET_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideErrorInNetwork) name:HAS_INTERNET_NOTIFICATION object:nil];
}

- (void)removeNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NO_INTERNET_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:HAS_INTERNET_NOTIFICATION object:nil];
}

#pragma mark - ErrorNotification

- (void)showErrorInNetwork
{
    [SVProgressHUD showErrorWithStatus:@"Network Error"];
}

- (void)hideErrorInNetwork
{
    [SVProgressHUD dismiss];
}

@end