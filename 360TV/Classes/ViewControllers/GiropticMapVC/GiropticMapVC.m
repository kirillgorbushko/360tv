#import "GiropticMapVC.h"
#import <MapKit/MapKit.h>
#import "GiropticAnnotationView.h"
#import "CameraNetworkManager.h"
#import "GiropticEndPoints.h"

@interface GiropticMapVC ()

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIView *topBarContainer;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation GiropticMapVC

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   // [self placeAnnotationIfExist];
    [self addAnnotationAtCoordinate:CLLocationCoordinate2DMake(12.906524, 77.629180)];
//
//    [[CameraNetworkManager sharedManager] giropticPatchMetadataForfileUri:self.fileUri GPSLatitude:12.906524 GPSLongitude:77.629180 operationResult:^(BOOL success, id response, NSError *error) {
//        if (success) {
//            NSLog(@"MEtadata added");
//        }
//    }];
}

#pragma mark - IBActions

- (IBAction)saveTap:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)closeTap:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Private

- (void)placeAnnotationIfExist
{
    if (!self.fileUri.length) {
        return;
    }
  //  __weak GiropticMapVC *weakSelf = self;
    [[CameraNetworkManager sharedManager] giropticGetMetadataForfileUri:self.fileUri operationResult:^(BOOL success, id response, NSError *error) {
        if (success) {
            
        }
        
    }];
}

#pragma mark - MKMap methods

-(void)addAnnotationAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate: coordinate];
    [annotation setTitle: @"File name"];
    [self.mapView addAnnotation: annotation];
    
    MKCoordinateRegion newRegion;
    
    newRegion.center.latitude = coordinate.latitude;
    newRegion.center.longitude = coordinate.longitude;
    
    newRegion.span.latitudeDelta = 0.005f;
    newRegion.span.longitudeDelta = 0.005f;
    
    [self.mapView setRegion: newRegion animated: YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    
    static NSString *reuseId = @"pin";
    MKPinAnnotationView *pav = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
    if (!pav) {
        pav = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
        pav.draggable = YES;
        pav.canShowCallout = YES;
    } else {
        pav.annotation = annotation;
    }
    return pav;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState
{
    if (newState == MKAnnotationViewDragStateEnding) {
     //   CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;
    }
}

@end