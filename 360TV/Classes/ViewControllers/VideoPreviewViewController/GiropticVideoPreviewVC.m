//  iOS 360° Player
//  Copyright (C) 2015  Giroptic
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>

#import "GiropticVideoPreviewVC.h"
#import "Animation.h"

static NSInteger const BottomBarHeight = 49;
static NSInteger const TopBarHeightPortrait = 64;
static NSInteger const TopBarHeightLandscape = 44;

@interface GiropticVideoPreviewVC ()

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *playStopButton;
@property (weak, nonatomic) IBOutlet UISlider *videoProgressSlider;
@property (weak, nonatomic) IBOutlet UISlider *volumeSlider;
@property (weak, nonatomic) IBOutlet UIButton *gyroscopeButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *downloadingActivityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *volumeSliderWidth;
@property (weak, nonatomic) IBOutlet UIView *topBarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBrHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *totalTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentTimeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sphericalButtonTralingSpaceConstraint;

@property (assign, nonatomic) CGFloat currentTime;

@property (assign, nonatomic) NSInteger topBarHeight;
@property (assign, nonatomic) BOOL isShown;

@end

@implementation GiropticVideoPreviewVC

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupSlider];
    [self prepareUI];
    [self updateConstraintToInterfaceOrientation:self.interfaceOrientation];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if (!self.isShown) {
        [self animateAppearing];
        self.isShown = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self forceRotation];
}

#pragma mark - Rotation

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateConstraintToInterfaceOrientation:toInterfaceOrientation];
}

- (void)canRotate
{
    //dummy
}

- (void)forceRotation
{
    [[UIDevice currentDevice] setValue: [NSNumber numberWithInteger: UIInterfaceOrientationPortrait] forKey:@"orientation"];
}

#pragma mark - UI

- (void)updateConstraintToInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    __weak typeof(self) weakSelf = self;
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        self.topBarHeight = TopBarHeightLandscape;
    } else if (UIInterfaceOrientationIsPortrait(orientation)) {
        self.topBarHeight = TopBarHeightPortrait;
    }
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.topBrHeightConstraint.constant = self.topBarHeight;
        [weakSelf.bottomView layoutIfNeeded];
    }];
}

- (IBAction)tapGesture:(UITapGestureRecognizer *)sender
{
    [self hideBottomBar];
    [self hideTopBar];
}

- (void)hideBottomBar
{
    __weak typeof (self) weakSelf = self;
    BOOL hidden = self.bottomViewHeightConstraint.constant;
    CGFloat newHeight = hidden ? 0.0f : BottomBarHeight;
    [UIView animateWithDuration:0.20 animations:^{
        weakSelf.bottomViewHeightConstraint.constant = newHeight;
        [weakSelf.view layoutIfNeeded];
    }];
}

- (void)hideTopBar
{
    BOOL hidden = self.topBrHeightConstraint.constant;
    CGFloat newHeight = hidden ? 0.0f : self.topBarHeight;
    __weak typeof (self) weakSelf = self;
    [UIView animateWithDuration:0.2 animations:^{
        weakSelf.topBrHeightConstraint.constant = newHeight;
        [weakSelf.topBarView layoutIfNeeded];
    }];
}

#pragma mark - IBActions

- (IBAction)gyroscopeButtonPress:(UIButton *)sender
{
    sender.selected = !sender.selected;
    [self setGyroscopeActive:sender.selected];
}

- (IBAction)playStopButtonPress:(id)sender
{
    if ([self isPlaying]) {
        [super pauseVideo];
    } else {
        [super playVideo];
    }
    self.playStopButton.selected = !self.playStopButton.selected;
}

- (IBAction)planarView:(id)sender
{
    __weak typeof(self) weakSelf = self;
    if (self.viewModel == PlanarModel) {
        [self switchToModel:SphericalModel];
        [UIView animateWithDuration:0.2 animations:^{
            weakSelf.gyroscopeButton.hidden = NO;
            weakSelf.sphericalButtonTralingSpaceConstraint.constant = 46;
        }];
    } else {
        [self switchToModel:PlanarModel];
        [UIView animateWithDuration:0.2 animations:^{
            weakSelf.gyroscopeButton.hidden = YES;
            weakSelf.sphericalButtonTralingSpaceConstraint.constant = 8;
        }];
    }
}

- (IBAction)littlePlanetView:(id)sender
{
    if (self.viewModel == LittlePlanetModel) {
        [self switchToModel:SphericalModel];
    } else {
        [self switchToModel:LittlePlanetModel];
        self.gyroscopeButton.enabled = [self isGyroscopeEnabled];
    }
}

- (IBAction)backButtonDidPressed:(id)sender
{
    CABasicAnimation *fadeAnimation = [Animation fadeAnimFromValue:1 to:0 delegate:self];
    [self.sphereWindow.rootViewController.view.layer addAnimation:fadeAnimation forKey:@"fadeAnim"];
    self.sphereWindow.rootViewController.view.layer.opacity = 0.0f;
}

- (IBAction)seekBack10SecButtonPress:(id)sender
{
    if (self.currentTime > 10.) {
        [self.videoPlayer seekPositionAtProgress:(self.currentTime - 10.) withPlayingStatus:YES];
    }
}

#pragma mark - AnimationDelegate

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (anim == [self.sphereWindow.rootViewController.view.layer animationForKey:@"fadeAnim"]) {
        [self.sphereWindow.rootViewController.view.layer removeAllAnimations];
        self.sphereWindow = nil;
    }
}

#pragma mark - SPHVideoPlayerDelegate

- (void)isReadyToPlay
{
    [super isReadyToPlay];
    [self disableControls:NO];
}

- (void)progressDidUpdate:(CGFloat)progress
{
    [super progressDidUpdate:progress];
    
    if (self.isPlaying) {
        self.videoProgressSlider.value = progress;
        [self.downloadingActivityIndicator stopAnimating];
    }
}

- (void)progressTimeChanged:(CMTime)time
{
    [super progressTimeChanged:time];
}

- (void)downloadingProgress:(CGFloat)progress
{
    [super downloadingProgress:progress];
    
    if (progress >= (self.playedProgress)) {
        [self.downloadingActivityIndicator startAnimating];
    } else {
        [self.downloadingActivityIndicator stopAnimating];
    }
    if ((progress - self.playedProgress) > 0.03) {
        [self disableControls:NO];
        if (self.isPlaying) {
            [self.videoPlayer play];
        }
    }
}

- (void)playerDidChangeProgressTime:(CGFloat)time totalTime:(CGFloat)totalDuration
{
    [super playerDidChangeProgressTime:time totalTime:totalDuration];
    if (time < 10) {
        self.currentTimeLabel.text = [NSString stringWithFormat:@"0%.0f:00", time];
    } else {
        self.currentTimeLabel.text = [NSString stringWithFormat:@"%.0f:00", time];
    }
    self.totalTimeLabel.text = [NSString stringWithFormat:@"%.2f", totalDuration];
    self.currentTime = time;
}

#pragma mark - UIConfiguration

- (void)setupSlider
{
    [self.videoProgressSlider addTarget:self action:@selector(progressSliderTouchedUp) forControlEvents:UIControlEventTouchUpInside];
    [self.volumeSlider addTarget:self action:@selector(volumeSliderTouchedUp) forControlEvents:UIControlEventValueChanged];
}

#pragma mark - Slider

- (void)progressSliderTouchedUp
{
    [self.videoPlayer pause];
    [self.downloadingActivityIndicator startAnimating];
    [self.videoPlayer seekPositionAtProgress:self.videoProgressSlider.value withPlayingStatus:self.isPlaying];
}

- (void)volumeSliderTouchedUp
{
    [self.videoPlayer setPlayerVolume:self.volumeSlider.value];
}

#pragma mark - Private

- (void)animateAppearing
{
    self.view.layer.opacity = 0.f;
    CABasicAnimation *fadeAnimation = [Animation fadeAnimFromValue:0 to:1. delegate:nil];
    [self.view.layer addAnimation:fadeAnimation forKey:nil];
    self.view.layer.opacity = 1.f;
}

- (void)prepareUI
{
    self.gyroscopeButton.enabled = [self isGyroscopeEnabled];
    [self.downloadingActivityIndicator startAnimating];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.topBarHeight = self.topBarView.frame.size.height;
}

- (void)disableControls:(BOOL)disable
{
    self.playStopButton.enabled = !disable;
    self.videoProgressSlider.enabled = !disable;
    self.volumeSlider.enabled = !disable;
    if (disable) {
        [self.downloadingActivityIndicator startAnimating];
    } else {
        [self.downloadingActivityIndicator stopAnimating];
    }
}

@end
