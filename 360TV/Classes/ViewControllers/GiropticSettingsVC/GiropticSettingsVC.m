#import "GiropticSettingsVC.h"
#import "UIViewController+JDSideMenu.h"
#import "SettingsDropDownTableCell.h"
#import "GiropticSwitchControl.h"
#import "UIColor+AppDefaultColors.h"
#import "GiropticSettingsParameters.h"
#import "CameraNetworkManager.h"
#import "AppDelegate.h"

static NSInteger const DropDownTableViewHeight = 100;
static NSString *const CellIdentifier = @"SettingsDropDownTableCell";

typedef NS_ENUM (NSUInteger, DropDownType) {
    DropDownTypeVideoWhiteBalance,
    DropDownTypeVideoFPS,
    DropDownTypeVideoBitrate,
    DropDownTypeVideoStabilisation,
    DropDownTypePhotoModeTimer,
    DropDownTypeBurstModeInterval,
    DropDownTypeTimelapseModeInterval,
    DropDownTypeNone
};

@interface GiropticSettingsVC () <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, UIAlertViewDelegate, UIScrollViewDelegate, SwitchControlDelegate>

@property (weak, nonatomic) IBOutlet UIView *tobBarContainer;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewSettings;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIView *generalSettingsView;
@property (weak, nonatomic) IBOutlet UIView *videoModeSettingsView;
@property (weak, nonatomic) IBOutlet UIView *photoModeSettingsView;
@property (weak, nonatomic) IBOutlet UIView *timelapseSettingsView;
@property (weak, nonatomic) IBOutlet UIView *burstSettingsView;

@property (weak, nonatomic) IBOutlet GiropticSwitchControl *gpsdataSwitch;
@property (weak, nonatomic) IBOutlet GiropticSwitchControl *gyroscopeSwitch;
@property (weak, nonatomic) IBOutlet GiropticSwitchControl *hdrSwitch;

@property (weak, nonatomic) IBOutlet UIView *modeSelectionView;
@property (weak, nonatomic) IBOutlet UIView *fpsSelectionView;
@property (weak, nonatomic) IBOutlet UIView *autocenteringSelectionView;
@property (weak, nonatomic) IBOutlet UIView *positionDisplaySelectionView;

@property (weak, nonatomic) IBOutlet UIView *selfTimerPhotoMode;
@property (weak, nonatomic) IBOutlet UIView *intervalTimelapseMode;
@property (weak, nonatomic) IBOutlet UIView *intervalBurstMode;
@property (weak, nonatomic) IBOutlet UITableView *dropDownTableView;

@property (weak, nonatomic) IBOutlet UIButton *leftMenuButton;

@property (weak, nonatomic) IBOutlet UILabel *modeSelectionLbl;
@property (weak, nonatomic) IBOutlet UILabel *fpsLbl;
@property (weak, nonatomic) IBOutlet UILabel *bitrateLabel;
@property (weak, nonatomic) IBOutlet UILabel *photoStabilisationLabel;
@property (weak, nonatomic) IBOutlet UILabel *photoExposureDelayLabel;
@property (weak, nonatomic) IBOutlet UILabel *burstIntervalLbl;
@property (weak, nonatomic) IBOutlet UILabel *timelapseIntervalLbl;
@property (weak, nonatomic) IBOutlet UIView *activityView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dropWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dropTopPositionConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dropHeightConstraint;

@property (assign, nonatomic) DropDownType dropdownType;
@property (strong, nonatomic) NSArray *dropDownListArray;
@property (strong, nonatomic) NSArray *composedDataSourceForDropDownList;

@property (strong, nonatomic) NSMutableDictionary *currecntSettings;

@end

@implementation GiropticSettingsVC

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addTapGestures];
    [self prepareUI];
    [self prepareDataSourceForDropDownList];
    [self setupSwitches];
    
    self.contentView.frame = self.view.frame;
    self.scrollViewSettings.contentSize = self.contentView.frame.size;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getCurrentOptionFromCamera];
    if (self.shownFromModify) {
        UIImage *image = [UIImage imageNamed:@"back"];
        [self.leftMenuButton setImage:image forState:UIControlStateNormal];
        [self.leftMenuButton setContentEdgeInsets:UIEdgeInsetsMake(30, 20, 16, 0)];
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.dropDownTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.dropDownTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.dropDownTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.dropDownTableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - IBActions

- (IBAction)leftMenuTapped:(id)sender
{
    if (self.shownFromModify) {
        [self close];
    } else {
        [self.sideMenuController showMenuAnimated:YES];
    }
}

- (IBAction)gpsSwitchTapped:(id)sender
{
    NSDictionary *parameters = @{GiroOptionKeyGPS : [NSNumber numberWithBool:self.gpsdataSwitch.isOn]};
    [self setOption:parameters];
}

- (IBAction)gyroscopeSwitchTapped:(id)sender
{
    NSDictionary *parameters = @{GiroOptionKeyGYRO : [NSNumber numberWithBool:self.gyroscopeSwitch.isOn]};
    [self setOption:parameters];
}

- (IBAction)hdrSwitchTapped:(id)sender
{
    NSDictionary *parameters = @{GiroOptionKeyHDR : [NSNumber numberWithBool:self.hdrSwitch.isOn]};
    [self setOption:parameters];
}

- (void)close
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SwitchControlDelegate

- (void)valueChanged:(GiropticSwitchControl *)sender
{
    if (sender == self.gpsdataSwitch) {
        [self gpsSwitchTapped:sender];
    } else if (sender == self.gyroscopeSwitch) {
        [self gyroscopeSwitchTapped:sender];
    } else if (sender == self.hdrSwitch) {
        [self hdrSwitchTapped:sender];
    }
}

#pragma mark - ComboBoxMenuSelection

- (void)positionDropDownViewUnder:(CGRect)topViewFrame parentViewFrame:(CGRect)parentViewFrame
{
    if (self.dropDownTableView.hidden) {
        CGRect currentRect = CGRectMake(topViewFrame.origin.x, parentViewFrame.origin.y + topViewFrame.origin.y + topViewFrame.size.height, topViewFrame.size.width, 0);
        [self showDropDownTableViewWithRect:currentRect];
    } else {
        [self hideDropDownTableView:nil];
    }
}

- (void)showDropDownTableViewWithRect:(CGRect)currentRect
{
    [self updateDataSourceForDropDownMenu];
    [self.dropDownTableView reloadData];
    
    self.dropDownTableView.hidden = NO;
    self.dropDownTableView.frame = currentRect;

    self.dropWidthConstraint.constant = currentRect.size.width;
    self.dropTopPositionConstraint.constant = currentRect.origin.y;

    if (self.dropdownType == DropDownTypeBurstModeInterval) {
        if (self.scrollViewSettings.contentSize.height + DropDownTableViewHeight >= [UIScreen mainScreen].bounds.size.height) {
            self.scrollViewSettings.contentSize = CGSizeMake(self.scrollViewSettings.contentSize.width, self.scrollViewSettings.contentSize.height + DropDownTableViewHeight);
            CGRect rect = CGRectZero;
            rect.size = self.scrollViewSettings.frame.size;
            self.contentView.frame = rect;
            [self.scrollViewSettings scrollRectToVisible:CGRectMake(0, self.scrollViewSettings.contentSize.height - DropDownTableViewHeight, 1, DropDownTableViewHeight) animated:YES];
        }
    }
    __block CGFloat height = DropDownTableViewHeight;
    if ([self.dropDownTableView numberOfRowsInSection:0] < 3) {
        height *= 0.66;
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        self.dropHeightConstraint.constant = height;
        [self.dropDownTableView layoutIfNeeded];
    }];
}

- (void)hideDropDownTableView:(void(^)())completitionHandler
{
    CGRect newRect = self.dropDownTableView.frame;
    newRect.size.height = 0;
    
    
    __weak GiropticSettingsVC *weakSelf = self;
    [UIView animateWithDuration:0.2 animations:^{
        weakSelf.dropHeightConstraint.constant = 0;
        [weakSelf.dropDownTableView layoutIfNeeded];
        
        if (weakSelf.dropdownType == DropDownTypeBurstModeInterval) {
            if (self.scrollViewSettings.contentSize.height + DropDownTableViewHeight >= [UIScreen mainScreen].bounds.size.height) {
                weakSelf.scrollViewSettings.contentSize = CGSizeMake(weakSelf.scrollViewSettings.contentSize.width, weakSelf.scrollViewSettings.contentSize.height - DropDownTableViewHeight);
            }
            weakSelf.dropdownType = DropDownTypeNone;
        }
    } completion:^(BOOL finished) {
        weakSelf.dropDownTableView.hidden = YES;
        if (completitionHandler) {
            completitionHandler();
        }
    }];
}

- (void)sensorTap
{
    if (self.dropdownType == DropDownTypeBurstModeInterval) {
        [self hideDropDownTableView:^{
            self.dropdownType = DropDownTypeVideoWhiteBalance;
            [self positionDropDownViewUnder: self.modeSelectionView.frame parentViewFrame:self.videoModeSettingsView.frame];
        }];
    } else {
        self.dropdownType = DropDownTypeVideoWhiteBalance;
        
        [self positionDropDownViewUnder: self.modeSelectionView.frame parentViewFrame:self.videoModeSettingsView.frame];
    }
}

- (void)fpsTap
{
    if (self.dropdownType == DropDownTypeBurstModeInterval) {
        [self hideDropDownTableView:^{
            self.dropdownType = DropDownTypeVideoFPS;
            [self positionDropDownViewUnder: self.fpsSelectionView.frame parentViewFrame:self.videoModeSettingsView.frame];
        }];
    } else {
        self.dropdownType = DropDownTypeVideoFPS;
        [self positionDropDownViewUnder: self.fpsSelectionView.frame parentViewFrame:self.videoModeSettingsView.frame];
    }
}

- (void)autoCenteringTap
{
    if (self.dropdownType == DropDownTypeBurstModeInterval) {
        [self hideDropDownTableView:^{
            self.dropdownType = DropDownTypeVideoBitrate;
            [self positionDropDownViewUnder: self.autocenteringSelectionView.frame parentViewFrame:self.videoModeSettingsView.frame];
        }];
    } else {
        self.dropdownType = DropDownTypeVideoBitrate;
        [self positionDropDownViewUnder: self.autocenteringSelectionView.frame parentViewFrame:self.videoModeSettingsView.frame];
    }
}

- (void)positionDisplayTap
{
    if (self.dropdownType == DropDownTypeBurstModeInterval) {
        [self hideDropDownTableView:^{
            self.dropdownType = DropDownTypeVideoStabilisation;
            [self positionDropDownViewUnder: self.positionDisplaySelectionView.frame parentViewFrame:self.photoModeSettingsView.frame];
        }];
    } else {
        self.dropdownType = DropDownTypeVideoStabilisation;
        [self positionDropDownViewUnder: self.positionDisplaySelectionView.frame parentViewFrame:self.videoModeSettingsView.frame];
    }
 }

- (void)photoModeTimerTap
{
    if (self.dropdownType == DropDownTypeBurstModeInterval) {
        [self hideDropDownTableView:^{
            self.dropdownType = DropDownTypePhotoModeTimer;
            [self positionDropDownViewUnder: self.selfTimerPhotoMode.frame parentViewFrame:self.photoModeSettingsView.frame];
        }];
    } else {
        self.dropdownType = DropDownTypePhotoModeTimer;
        [self positionDropDownViewUnder: self.selfTimerPhotoMode.frame parentViewFrame:self.photoModeSettingsView.frame];
    }
}

- (void)timelapseModeIntervalTap
{
    if (self.dropdownType == DropDownTypeBurstModeInterval) {
        [self hideDropDownTableView:^{
            self.dropdownType = DropDownTypeTimelapseModeInterval;
            [self positionDropDownViewUnder: self.intervalTimelapseMode.frame parentViewFrame:self.timelapseSettingsView.frame];
        }];
    } else {
        self.dropdownType = DropDownTypeTimelapseModeInterval;
        [self positionDropDownViewUnder: self.intervalTimelapseMode.frame parentViewFrame:self.timelapseSettingsView.frame];
    }
}

- (void)burstModeIntervalTap
{
    if (!self.dropDownTableView.hidden && self.dropdownType != DropDownTypeBurstModeInterval) {
        [self hideDropDownTableView:^{
            self.dropdownType = DropDownTypeBurstModeInterval;
            [self positionDropDownViewUnder:self.intervalBurstMode.frame parentViewFrame: self.burstSettingsView.frame];
        }];
    } else {
        self.dropdownType = DropDownTypeBurstModeInterval;
        [self positionDropDownViewUnder:self.intervalBurstMode.frame parentViewFrame: self.burstSettingsView.frame];
    }
}

- (void)tapOnView:(UITapGestureRecognizer *)recognizer
{
    if (!self.dropDownTableView.hidden) {
        [self hideDropDownTableView:nil];
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.dropDownTableView]) {
        return NO;
    }
    return YES;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self getCurrentOptionFromCamera];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dropDownListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsDropDownTableCell *cell = (SettingsDropDownTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[SettingsDropDownTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.titleLbl.text = [self.dropDownListArray objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *parameters;
    switch (self.dropdownType)
    {
        case DropDownTypeVideoWhiteBalance: {
            NSString *selection = [self.dropDownListArray objectAtIndex:indexPath.row];
            self.modeSelectionLbl.text = selection;
            parameters = @{GiroOptionKeyWhiteBalance: [self selectedVideoModeFromUI:selection]};
            break;
        }
        case DropDownTypeVideoFPS: {
            self.fpsLbl.text = [self.dropDownListArray objectAtIndex:indexPath.row];
            parameters = @{GiroOptionKeyVideoFrameRate : @([[self.dropDownListArray objectAtIndex:indexPath.row] integerValue])};
            break;
        }
        case DropDownTypeVideoBitrate: {
            self.bitrateLabel.text = [self.dropDownListArray objectAtIndex:indexPath.row];
            parameters = @{GiroOptionKeyVideoBitrate : @([[self.dropDownListArray objectAtIndex:indexPath.row] integerValue])};
            break;
        }
        case DropDownTypePhotoModeTimer: {
            self.photoExposureDelayLabel.text = [self.dropDownListArray objectAtIndex:indexPath.row];
            parameters = @{GiroOptionKeyExplosureDelay : @([[self.dropDownListArray objectAtIndex:indexPath.row] integerValue])};
            break;
        }
        case DropDownTypeBurstModeInterval: {
            self.burstIntervalLbl.text = [self.dropDownListArray objectAtIndex:indexPath.row];
            NSDictionary *parameters = @{GiroOptionKeyBurstDuration : @([[self.dropDownListArray objectAtIndex:indexPath.row] integerValue])};
            [self setOption:parameters];
            break;
        }
        case DropDownTypeTimelapseModeInterval: {
            self.timelapseIntervalLbl.text = [self.dropDownListArray objectAtIndex:indexPath.row];
            parameters = @{GiroOptionKeyTimeLapsInterval: @([[self.dropDownListArray objectAtIndex:indexPath.row] integerValue])};
            break;
        }
        case DropDownTypeVideoStabilisation: {
            NSString *selection = [self.dropDownListArray objectAtIndex:indexPath.row];
            self.photoStabilisationLabel.text = selection;
            parameters = @{GiroOptionKeyImageStabilisation: [self selectedImageStabilisationFromUI:selection]};
            break;
        }
        default:
            break;
    }
    [self setOption:parameters];
    [self hideDropDownTableView:nil];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectZero];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - APIUsage

- (void)getCurrentOptionFromCamera
{
    NSArray *requestedOptions = @[
                                  GiroOptionKeyCaptureMode,
                                  GiroOptionKeyGPS,
                                  GiroOptionKeyGYRO,
                                  GiroOptionKeyHDR,
                                  GiroOptionKeyRemainingSpace,
                                  GiroOptionKeyTimeLapsInterval,
                                  GiroOptionKeyVideoBitrate,
                                  GiroOptionKeyVideoFrameRate,
                                  GiroOptionKeyWhiteBalance,
                                  GiroOptionKeyTotalSpace,
                                  GiroOptionKeyFileFormat,
                                  GiroOptionKeyButteryLevel,
                                  GiroOptionKeyBurstDuration,
                                  GiroOptionKeyExplosureDelay,
                                  GiroOptionKeyImageStabilisation
                                  ];
    __block NSDictionary *responseWithOptions = [NSDictionary dictionary];
    __weak GiropticSettingsVC *weakSelf = self;
    [[CameraNetworkManager sharedManager] giropticGetOptions:requestedOptions operationResult:^(BOOL success, id response, NSError *error) {
        if (success) {
            responseWithOptions = [[response valueForKey:@"results"] valueForKey:@"options"];
            NSLog(@"%@", response);
            [weakSelf displayCurrentSettingOnUI:responseWithOptions];
        }
    }];
}

- (void)setOption:(NSDictionary *)dictionary
{
    self.activityView.hidden = NO;

    __weak GiropticSettingsVC *weakSelf = self;
    [[CameraNetworkManager sharedManager] giropticSetOptions:dictionary operationResult:^(BOOL success, id response, NSError *error) {
        weakSelf.activityView.hidden = YES;
        if (success) {
            NSLog(@"Setting changed");
            NSArray *keys = [dictionary allKeys];
            [self.currecntSettings setValue:dictionary[keys[0]] forKey:keys[0]];
        } else {
            NSLog(@"Error - %@", error.localizedDescription);
            [[[UIAlertView alloc] initWithTitle:[AppDelegate appName] message:@"Cant change settings" delegate:weakSelf cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            [self displayCurrentSettingOnUI:self.currecntSettings];
        }
    }];
}

#pragma mark - Private

- (void)displayCurrentSettingOnUI:(NSDictionary *)responseDictionary
{
    self.currecntSettings = [responseDictionary mutableCopy];
    
    [self.gpsdataSwitch setOn:[[responseDictionary valueForKey:GiroOptionKeyGPS] integerValue]];
    [self.gyroscopeSwitch setOn:[[responseDictionary valueForKey:GiroOptionKeyGYRO] integerValue]];
    self.modeSelectionLbl.text = [self selectedVideoModefromResponse:[responseDictionary valueForKey:GiroOptionKeyWhiteBalance]];
    self.fpsLbl.text = [[responseDictionary valueForKey:GiroOptionKeyVideoFrameRate] stringValue];
    self.bitrateLabel.text = [[responseDictionary valueForKey:GiroOptionKeyVideoBitrate] stringValue];
    if ([responseDictionary valueForKey:GiroOptionKeyHDR]) {
        [self.hdrSwitch setOn:[[responseDictionary valueForKey:GiroOptionKeyHDR] integerValue]];
    }
    if ([[responseDictionary valueForKey:GiroOptionKeyCaptureMode] isEqualToString:@"_video"]) {
        self.photoExposureDelayLabel.text = @"";
    } else {
        self.photoExposureDelayLabel.text = [[responseDictionary valueForKey:GiroOptionKeyExplosureDelay] stringValue];
    }
    self.photoStabilisationLabel.text = [self selectedImageStabilisationFromResponse:[responseDictionary valueForKey:GiroOptionKeyImageStabilisation]];
    
    if ([responseDictionary valueForKey:GiroOptionKeyBurstDuration]) {
        self.burstIntervalLbl.text = [[responseDictionary valueForKey:GiroOptionKeyBurstDuration] stringValue];
    } else {
        self.burstIntervalLbl.text = @"";
    }
}

- (void)setupSwitches
{
    self.gpsdataSwitch.delegate = self;
    self.gyroscopeSwitch.delegate = self;
    self.hdrSwitch.delegate = self;
}

- (NSString *)selectedVideoModefromResponse:(NSString *)response
{
    NSString *mode;
    if ([response isEqualToString:@"_auto_wide"]) {
        mode = @"Auto wide";
    } else if ([response isEqualToString:@"_auto_normal"]) {
        mode = @"Auto normal";
    } else if ([response isEqualToString:@"_sunny"]) {
        mode = @"Sunny";
    } else if ([response isEqualToString:@"_shadow"]) {
        mode = @"Shadow";
    } else if ([response isEqualToString:@"_indoor"]) {
        mode = @"Indoor";
    }
    return mode;
}

- (NSString *)selectedVideoModeFromUI:(NSString *)selectedMode
{
    NSString *mode;
    if ([selectedMode isEqualToString:@"Auto wide"]) {
        mode = @"_auto_wide";
    } else if ([selectedMode isEqualToString:@"Auto normal"]) {
        mode = @"_auto_normal";
    } else if ([selectedMode isEqualToString:@"Sunny"]) {
        mode = @"_sunny";
    } else if ([selectedMode isEqualToString:@"Shadow"]) {
        mode = @"_shadow";
    } else if ([selectedMode isEqualToString:@"Indoor"]) {
        mode = @"_indoor";
    }
    return mode;
}

- (NSString *)selectedImageStabilisationFromResponse:(NSString *)response
{
    NSString *mode;
    if ([response isEqualToString:@"off"]) {
        mode = @"None";
    } else if ([response isEqualToString:@"_horizontal_stabilization"]) {
        mode = @"Activated";
    } else if ([response isEqualToString:@"_vibration_correction"]) {
        mode = @"Activated";
    }
    return mode;
}

- (NSString *)selectedImageStabilisationFromUI:(NSString *)selection
{
    NSString *mode;
    if ([selection isEqualToString:@"None"]) {
        mode = @"off";
    } else if ([selection isEqualToString:@"Activated"]) {
        mode = @"_horizontal_stabilization";
    } else {
        mode = @"_vibration_correction";
    }
    return mode;
}

- (void)prepareDataSourceForDropDownList
{
    self.composedDataSourceForDropDownList= [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"SettingsModes" ofType:@"plist"]];
}

- (void)prepareUI
{
    self.dropDownTableView.layer.borderWidth = 0.5;
    self.dropDownTableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.dropDownTableView.clipsToBounds = YES;
    self.view.layer.borderWidth = 0.5;
    self.view.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)addTapGestures
{
    UITapGestureRecognizer *sensorTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sensorTap)];
    [self.modeSelectionView addGestureRecognizer:sensorTap];
    
    UITapGestureRecognizer *fpsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(fpsTap)];
    [self.fpsSelectionView addGestureRecognizer:fpsTap];
    
    UITapGestureRecognizer *autocenterTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(autoCenteringTap)];
    [self.autocenteringSelectionView addGestureRecognizer:autocenterTap];
    
    UITapGestureRecognizer *positiondisplayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(positionDisplayTap)];
    [self.positionDisplaySelectionView addGestureRecognizer:positiondisplayTap];
    
    UITapGestureRecognizer *photoTimerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoModeTimerTap)];
    [self.selfTimerPhotoMode addGestureRecognizer:photoTimerTap];
    
    UITapGestureRecognizer *intervalBurstTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(burstModeIntervalTap)];
    [self.intervalBurstMode addGestureRecognizer:intervalBurstTap];
    
    UITapGestureRecognizer *intervalTimelapseTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(timelapseModeIntervalTap)];
    [self.intervalTimelapseMode addGestureRecognizer:intervalTimelapseTap];
    
    UITapGestureRecognizer *contentViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnView:)];
    contentViewTap.delegate = self;
    [self.scrollViewSettings addGestureRecognizer:contentViewTap];
    
    if(!isIPad()) [self.leftMenuButton addGestureRecognizer:self.sideMenuController.tapRecognizer];
}

- (void)updateDataSourceForDropDownMenu
{
    switch (self.dropdownType) {
        case DropDownTypeVideoWhiteBalance: {
            self.dropDownListArray = [[self.composedDataSourceForDropDownList valueForKey:GiroOptionKeyWhiteBalanceSupport] firstObject];
            break;
        }
        case DropDownTypeVideoFPS: {
            self.dropDownListArray = [[self.composedDataSourceForDropDownList valueForKey:GiroOptionKeyVideoFrameRateSupport] firstObject];
            break;
        }
        case DropDownTypeVideoBitrate: {
            self.dropDownListArray = [[self.composedDataSourceForDropDownList valueForKey:GiroOptionKeyVideoBitrateSupport] firstObject];
            break;
        }
        case DropDownTypeVideoStabilisation:{
            self.dropDownListArray = [[self.composedDataSourceForDropDownList valueForKey:GiroOptionKeyImageStabilisationSupport] firstObject];
            break;
        }
        case DropDownTypePhotoModeTimer:{
            self.dropDownListArray = [[self.composedDataSourceForDropDownList valueForKey:GiroOptionKeyExplosureDelaySupport] firstObject];
            break;
        }
        case DropDownTypeBurstModeInterval:{
            self.dropDownListArray = [[self.composedDataSourceForDropDownList valueForKey:GiroOptionKeyBurstDurationSupport] firstObject];
            break;
        }
        case DropDownTypeTimelapseModeInterval:{
            self.dropDownListArray = [[self.composedDataSourceForDropDownList valueForKey:GiroOptionKeyTimeLapsIntervalSupport] firstObject];
            break;
        }
        default:
            break;
    }
}

@end