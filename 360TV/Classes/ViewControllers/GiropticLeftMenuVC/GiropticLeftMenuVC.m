#import "GiropticLeftMenuVC.h"
#import "UIViewController+JDSideMenu.h"
#import "GiropticHomeVC.h"
#import "GiropticMediaPreviewVC.h"
#import "GiropticSettingsVC.h"
#import "GiropticLeftMenuTableCell.h"
#import "AppDelegate.h"
#import "DetailViewManager.h"
#import "MainPreviewViewController.h"
#import "UIColor+AppDefaultColors.h"
#import "CameraNetworkManager.h"
#import "CameraNetworkManager.h"
#import "WiFiFetchService.h"

static NSString *const CellIdentifierBottomTableView = @"LeftMenuTableViewCell";
static NSString *const CellIdentifierTopTableView = @"OfflineLeftMenuTableViewCell";
static NSString *const GiropticMediaPreviewVCName = @"GiropticMediaPreviewVC";
static NSString *const GiropticGiropticSettingsVC = @"GiropticSettingsVC";

@interface GiropticLeftMenuVC () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *menuTableView;
@property (nonatomic, weak) IBOutlet UITableView *offlineMenuTableView;
@property (nonatomic, weak) IBOutlet UIView *cameraNameView;
@property (nonatomic, weak) IBOutlet UILabel *cameraNameLabel;
@property (nonatomic, weak) IBOutlet UIImageView *cameraNameDropDownImg;
@property (weak, nonatomic) IBOutlet UIButton *closeMenuButton;

@property (nonatomic, strong) NSArray *menuItemsArray;
@property (nonatomic, strong) NSArray *offlineMenuItemsArray;
@property (strong, nonatomic) NSIndexPath *prevSelectedCells;

@property (strong, nonatomic) UIViewController *container;

@end

@implementation GiropticLeftMenuVC

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setDeviceOrientation];
    [self prepareUI];
    [self prepareGesture];
    [self configureDataSourceForTableViews];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateComboBox];
    
    if (isIPad() && [[CameraNetworkManager sharedManager] isCameraConnected]) {
        self.closeMenuButton.hidden = YES;
    } else {
        self.closeMenuButton.hidden = NO;
    }
}

#pragma mark - IBActions

- (IBAction)closeButtonPress:(id)sender
{
    [self.sideMenuController hideMenuAnimated:YES];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (tableView == self.offlineMenuTableView) ? self.offlineMenuItemsArray.count : self.menuItemsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GiropticLeftMenuTableCell *cell = (GiropticLeftMenuTableCell *)[tableView dequeueReusableCellWithIdentifier:(tableView == self.menuTableView) ? CellIdentifierBottomTableView : CellIdentifierTopTableView];
    if (!cell) {
        cell = [[GiropticLeftMenuTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:(tableView == self.menuTableView) ? CellIdentifierBottomTableView : CellIdentifierTopTableView];
    }

    if (tableView == self.menuTableView) {
        cell.menuImageView.image = [UIImage imageNamed:[self.menuItemsArray[indexPath.row] valueForKey:@"image"]];
        cell.menuTitleLabel.text = [self.menuItemsArray[indexPath.row] valueForKey:@"title"];
    } else {
        cell.menuImageView.image = [UIImage imageNamed:[self.offlineMenuItemsArray[indexPath.row] valueForKey:@"image"]];
        cell.menuTitleLabel.text = [self.offlineMenuItemsArray[indexPath.row] valueForKey:@"title"];
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.prevSelectedCells) {
        if (tableView == self.offlineMenuTableView) {
            [self.menuTableView deselectRowAtIndexPath:self.prevSelectedCells animated:NO];
        } else {
            [self.offlineMenuTableView deselectRowAtIndexPath:self.prevSelectedCells animated:NO];
        }
    }
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self saveHomeViewControllerForSelectedIndexPath:indexPath];
    
    switch (indexPath.row) {
        case 0: {
            if ([self.sideMenuController.contentController isKindOfClass:[UINavigationController class]]) {
                for (UIViewController *viewController in ((UINavigationController *)self.sideMenuController.contentController).viewControllers) {
                    if ([viewController isKindOfClass:[GiropticHomeVC class]] || [viewController isKindOfClass:[MainPreviewViewController class]]) {
                        [self.sideMenuController hideMenuAnimated:YES];
                        self.prevSelectedCells = indexPath;
                        return;
                    }
                }
            }
            [self showHomeViewController];
            break;
        }
        case 1: {
            if (tableView == self.offlineMenuTableView) {
                [self showViewControllerWithName:GiropticMediaPreviewVCName viewMode:ControllerViewModeCameraRoll];
            } else  {
                [self showViewControllerWithName:GiropticMediaPreviewVCName viewMode:ControllerViewModeSDCard];
            }
            break;
        } case 2: {
            if (tableView == self.offlineMenuTableView) {
                [[[UIAlertView alloc] initWithTitle:[AppDelegate appName] message:@"Help currently not avalibale" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            }
//   DO NOT UNCOMMENT
//            break;
//        } case 3: {
            if (tableView == self.menuTableView) {
                [self showViewControllerWithName:GiropticGiropticSettingsVC viewMode:ControllerViewModeNone];
            }
            break;
        }
        default: {
            break;
        }
    }
    
    self.prevSelectedCells = indexPath;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectZero];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - Private

- (void)saveHomeViewControllerForSelectedIndexPath:(NSIndexPath *)indexPath
{
    if (isIPad() && [[CameraNetworkManager sharedManager] isCameraConnected]) {
        UIViewController *vc = ((DetailViewManager*)self.splitViewController.delegate).detailViewController;
        if ([vc isKindOfClass:[UINavigationController class]]) {
            NSArray *a = ((UINavigationController *)vc).viewControllers;
            for (UIViewController *viewController in a) {
                if ([viewController isKindOfClass:[GiropticHomeVC class]]){
                    self.container = viewController;
                }
            }
        }
    } else {
        if (!self.sideMenuController.tempContainer) {
            NSArray *viewControllers = ((UINavigationController *)self.sideMenuController.contentController).viewControllers;
            if (indexPath.row && [viewControllers[0] isKindOfClass:[GiropticHomeVC class]]) {
                self.sideMenuController.tempContainer = viewControllers[0];
            } else if (indexPath.row && [viewControllers[0] isKindOfClass:[MainPreviewViewController class]]) {
                self.sideMenuController.tempContainer = viewControllers[0];
            }
        }
    }
}

#pragma mark - UI

- (void)updateComboBox
{
    if ([CameraNetworkManager sharedManager].isSessionActive && [WiFiFetchService sharedService].cameraName.length) {
        self.cameraNameLabel.text = [WiFiFetchService sharedService].cameraName;
        self.menuTableView.hidden = NO;
        self.cameraNameDropDownImg.image = [UIImage imageNamed:@"down_carat_grey"];
    } else {
        self.cameraNameLabel.text = @"CONNECT TO 360CAM";
        self.menuTableView.hidden = YES;
        self.cameraNameDropDownImg.image = [UIImage imageNamed:@"up_carat_grey"];
    }
}

- (void)openCameraMenu
{
    if ([[CameraNetworkManager sharedManager] isCameraConnected]) {
        self.menuTableView.hidden = !self.menuTableView.hidden;
        self.cameraNameDropDownImg.image = (self.menuTableView.hidden) ? [UIImage imageNamed:@"up_carat_grey"] : [UIImage imageNamed:@"down_carat_grey"];
        [self.menuTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else {
        self.cameraNameDropDownImg.image = [UIImage imageNamed:@"up_carat_grey"];
        [[[UIAlertView alloc] initWithTitle:[AppDelegate appName] message:@"Please connect to Giroptic 360cam's wifi from Settings app" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:@"Open Settings",nil] show];
    }
}

#pragma mark - Preparation

- (void)setDeviceOrientation
{
    if (!isIPad()) {
        if (self.interfaceOrientation != UIInterfaceOrientationPortrait) {
            [[UIDevice currentDevice] performSelector:@selector(setOrientation:) withObject:(__bridge id)((void*)UIInterfaceOrientationPortrait)];
        }
    }
}

- (void)configureDataSourceForTableViews
{
    self.offlineMenuItemsArray = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MainMenu" ofType:@"plist"]];
    self.menuItemsArray = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Menu_En" ofType:@"plist"]];
}

- (void)prepareUI
{
    if ([self.menuTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.menuTableView setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)prepareGesture
{
    UITapGestureRecognizer *cameraTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openCameraMenu)];
    [self.cameraNameView addGestureRecognizer:cameraTap];
}

#pragma mark - Navigation

- (void)showViewControllerWithName:(NSString *)viewControllerName viewMode:(ControllerViewMode)controllerMode
{
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:viewControllerName];
    if ([viewControllerName isEqualToString:GiropticMediaPreviewVCName]) {
        ((GiropticMediaPreviewVC *)viewController).photoViewMode = controllerMode;
    }
    UINavigationController *navc = [[UINavigationController alloc] initWithRootViewController:viewController];
    navc.navigationBarHidden = YES;
    
    if (isIPad() && [[CameraNetworkManager sharedManager] isCameraConnected]) {
        UIViewController <SubstitutableDetailViewController> *detailViewController = (id)navc;
        [self navigateToViewController:detailViewController];
    } else {
        [self.sideMenuController setContentController:navc animated:NO];
    }
}

- (void)showHomeViewController
{
    if (isIPad() && [[CameraNetworkManager sharedManager] isCameraConnected]) {
        [self navigateToViewController:self.container];
    } else {
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:self.sideMenuController.tempContainer];
        navController.navigationBarHidden = YES;
        UIViewController *leftMenuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LeftMenuVC"];
        JDSideMenu *sideMenu = [[JDSideMenu alloc] initWithContentController:navController menuController:leftMenuVC];
        [self.sideMenuController setContentController:sideMenu animated:NO];
    }
}

- (void)navigateToViewController:(UIViewController *)viewControllerToPresent
{
    DetailViewManager *detailViewManager = (DetailViewManager*)self.splitViewController.delegate;
    detailViewManager.detailViewController = (id)viewControllerToPresent;
}

@end