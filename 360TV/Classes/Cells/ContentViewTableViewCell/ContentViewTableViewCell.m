//
//  ContentViewTableViewCell.m
//  360cam
//
//  Created by Kirill Gorbushko on 03.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import "ContentViewTableViewCell.h"

@implementation ContentViewTableViewCell

#pragma mark - LifeCycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.previewImageView.image = nil;
    self.previewLabel.text = @"";
    [self prepareShadow];
}

#pragma mark - Private

- (void)prepareShadow
{
    self.containerView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.containerView.layer.shadowOffset = CGSizeMake(1., 1.);
    self.containerView.layer.shadowOpacity = 0.5;
    self.contentView.clipsToBounds = NO;
}

@end
