//
//  ContentViewTableViewCell.h
//  360cam
//
//  Created by Kirill Gorbushko on 03.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentViewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *previewImageView;
@property (weak, nonatomic) IBOutlet UILabel *previewLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end
