#import "GiropticMediaFile.h"

@class GiropticMediaListTableCell;
@class ALAsset;

@protocol GiropticMediaListTableCellDelegate <NSObject>

@optional
- (void)shareButtonDidTappedForCell:(GiropticMediaListTableCell *)cell;
- (void)downloadButtonDidTappedForCell:(GiropticMediaListTableCell *)cell;
- (void)infoButtonDidTappedForCell:(GiropticMediaListTableCell *)cell;

@end

@interface GiropticMediaListTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mediaImgView;
@property (weak, nonatomic) IBOutlet UIImageView *mediaTypeImgView;
@property (weak, nonatomic) IBOutlet UILabel *fileNameLbl;
@property (weak, nonatomic) IBOutlet UIButton *shareBtn;
@property (weak, nonatomic) IBOutlet UIButton *downloadBtn;
@property (weak, nonatomic) IBOutlet UIButton *infoBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tralingInfoConstraint;
@property (weak, nonatomic) IBOutlet UILabel *previewLabel;


@property (weak, nonatomic) id <GiropticMediaListTableCellDelegate> delegate;

- (void)prepareUIWith:(GiropticMediaFile *)mediaFile;
- (void)prepareUIWithALAsset:(ALAsset *)asset;

@end
