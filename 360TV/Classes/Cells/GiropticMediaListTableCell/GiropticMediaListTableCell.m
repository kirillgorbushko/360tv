#import "GiropticMediaListTableCell.h"
#import "AppDelegate.h"
#import <AssetsLibrary/AssetsLibrary.h>

@implementation GiropticMediaListTableCell

#pragma mark - LifeCycle

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.mediaImgView.image = nil;
    self.mediaTypeImgView.image = nil;
    self.fileNameLbl.text = @"";
    self.downloadBtn.hidden = NO;
    self.tralingInfoConstraint.constant = 50;
    self.previewLabel.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:NO animated:NO];
}

#pragma mark - Public

- (void)prepareUIWith:(GiropticMediaFile *)mediaFile
{
    [self hideDownloadButton];
    
    switch (mediaFile.fileType) {
        case FileTypeJPG: {
            self.mediaTypeImgView.image = [UIImage imageNamed:@"mediaType_photo"];
            break;
        }
        case FileTypeHDR: {
            self.mediaTypeImgView.image = [UIImage imageNamed:@"mediaType_photo"];
            break;
        }
        case FileTypeBURST: {
            self.mediaTypeImgView.image = [UIImage imageNamed:@"mediaType_burst"];
            break;
        }
        case FileTypeTIMELAPS: {
            self.mediaTypeImgView.image = [UIImage imageNamed:@"mediaType_timelapse"];
            break;
        }
        case FileTypeVIDEO: {
            self.mediaTypeImgView.image = [UIImage imageNamed:@"Video-white"];
            break;
        }
        default:
            break;
    }
}

- (void)prepareUIWithALAsset:(ALAsset *)asset
{
    [self hideDownloadButton];
    if ([asset.defaultRepresentation.filename hasSuffix:@"JPG"]) {
        self.mediaTypeImgView.image = [UIImage imageNamed:@"mediaType_photo"];
    } else if ([asset.defaultRepresentation.filename hasSuffix:@"MP4"]) {
        self.mediaTypeImgView.image = [UIImage imageNamed:@"Video-white"];
    }
}

#pragma mark - Private

- (void)hideDownloadButton
{
    self.tralingInfoConstraint.constant = 10;
    self.downloadBtn.hidden = YES;
}

#pragma mark - GiropticMediaListTableCellDelegate

- (IBAction)shareTapped:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(shareButtonDidTappedForCell:)]) {
        [self.delegate shareButtonDidTappedForCell:self];
    }
}

- (IBAction)downloadTapped:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(downloadButtonDidTappedForCell:)]) {
        [self.delegate downloadButtonDidTappedForCell:self];
    }
}

- (IBAction)infoTapped:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(infoButtonDidTappedForCell:)]) {
        [self.delegate infoButtonDidTappedForCell:self];
    }
}

@end
