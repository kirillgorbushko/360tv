//
//  FileEntries.h
//  testCamera360Api
//
//  Created by Kirill Gorbushko on 19.03.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, FileType) {
    FileTypeJPG,
    FileTypeHDR,
    FileTypeBURST,
    FileTypeTIMELAPS,
    FileTypeVIDEO,
    FileTypeUNDEFINED
};

@interface GiropticMediaFile : NSObject

@property (assign, nonatomic) BOOL isLink;
@property (copy, nonatomic) NSString *kind;
@property (strong, nonatomic) NSDate *modificationTimeMs;
@property (copy, nonatomic) NSString *fileName;
@property (assign, nonatomic) long long int sizeInBytes;

@property (strong, nonatomic) NSURL *fileURL;
@property (assign, nonatomic) FileType fileType;


+ (GiropticMediaFile *)getFileFromDictionary:(NSDictionary *)inputDictionary;
+ (NSString *)getKeyForFile:(NSString *)fileObject suffix:(NSString *)keySuffix;

@end
