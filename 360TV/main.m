//
//  main.m
//  360TV
//
//  Created by Kirill Gorbushko on 16.03.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//


#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
