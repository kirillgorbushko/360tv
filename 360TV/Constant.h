//
//  Constant.h
//  360cam
//
//  Created by Kirill Gorbushko on 09.04.15.
//  Copyright (c) 2015 Kirill Gorbushko. All rights reserved.
//

static NSString *const RTSPPlayerShouldStopUpdateFrameInPhotoOpenGLPreview = @"RTSPPlayerShouldStopUpdateFrameInPhotoOpenGLPreview";

static NSString *const RTSPPlayerShouldStartUpdateFrameInPhotoOpenGLPreview = @"RTSPPlayerShouldStartUpdateFrameInPhotoOpenGLPreview";